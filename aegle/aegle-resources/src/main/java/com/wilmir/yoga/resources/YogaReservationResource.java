package com.wilmir.yoga.resources;


import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wilmir.yoga.common.exception.*;
import com.wilmir.yoga.dto.YogaReservation;
import com.wilmir.yoga.error.ErrorMessage;
import com.wilmir.yoga.error.ErrorMessages;
import com.wilmir.yoga.model.Secured;
import com.wilmir.yoga.model.YogaClass;
import com.wilmir.yoga.services.YogaClassService;

@Path("/yoga-reservations")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
public class YogaReservationResource {

	@Inject
	YogaClassService yogaClassService;
	  
	@POST
	@Secured
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response bookYogaClass(final YogaReservation dto) {						
		try {
			final YogaClass yogaClass = yogaClassService.bookAYogaClass(dto.getYogaClassId(), dto.getStudentId());
			return Response.status(200).entity(yogaClass).build();
		}catch(YogaClassNotFoundException exception){
			return Response.status(404).entity(new ErrorMessage(ErrorMessages.YOGA_CLASS_NOT_FOUND.getMessage())).build();
		}catch(YogaClassFullException exception) {
			return Response.status(403).entity(new ErrorMessage(ErrorMessages.YOGA_FULL.getMessage())).build();
		}catch(YogaClassOngoingException exception) {
			return Response.status(403).entity(new ErrorMessage(ErrorMessages.YOGA_ONGOING.getMessage())).build();
		}catch(UserNotFoundException exception) {
			return Response.status(404).entity(new ErrorMessage(ErrorMessages.USER_NOT_FOUND.getMessage())).build();
		}catch(UnauthorizedReservationException exception) {
			return Response.status(403).entity(new ErrorMessage(ErrorMessages.UNAUTHORIZED_RESERVATION.getMessage())).build();
		}catch(YogaClassAlreadyBookedException exception) {
			return Response.status(403).entity(new ErrorMessage(ErrorMessages.ALREADY_BOOKED.getMessage())).build();
		}
	}

}
