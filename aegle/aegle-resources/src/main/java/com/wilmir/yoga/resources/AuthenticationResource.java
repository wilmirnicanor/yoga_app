package com.wilmir.yoga.resources;

import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wilmir.yoga.dto.AuthenticationDTO;
import com.wilmir.yoga.dto.Credentials;
import com.wilmir.yoga.model.User;
import com.wilmir.yoga.resources.security.TokenUtil;
import com.wilmir.yoga.services.AuthService;

@Path("/auth")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
public class AuthenticationResource {

	@Inject
	AuthService authService;
	
	@POST
	@Path("/login")
	@Consumes({MediaType.APPLICATION_JSON})
	public Response authenticateUser(final Credentials credentials) {
		if(authService.authenticate(credentials.getEmail(), credentials.getPassword())){
			final Optional<User> user = authService.getUser(credentials.getEmail());
			final AuthenticationDTO authDTO = new AuthenticationDTO(user.get());
			TokenUtil.issueNewToken(authDTO);
			return Response.status(Response.Status.OK).entity(authDTO).build();
		}else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}	
	}

	@GET
	@Path("/logout/{token}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response logout(@PathParam("token") final String token) {
		TokenUtil.revokeToken(token);
		return Response.ok().build();
	}
}
