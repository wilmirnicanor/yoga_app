package com.wilmir.yoga.dto;

import java.io.Serializable;

import com.wilmir.yoga.model.YogaClass;

public class CreateYogaDTO implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 6955472011442654395L;

	private YogaClass yogaClass;
		
	public CreateYogaDTO() {}

	
	public CreateYogaDTO(final YogaClass yogaClass) {
		this.yogaClass = yogaClass;
	}

	public YogaClass getYogaClass() {
		return yogaClass;
	}
		
}
