package com.wilmir.yoga.error;

public enum ErrorMessages {
	OVERLAPPING_SCHEDULE("You have an overlapping class on the dates given"), 
	YOGA_CLASS_NOT_FOUND("The requested class is not found"),
	YOGA_FULL("The requested class is full"),
	YOGA_ONGOING("The requested class is on-going"),
	USER_NOT_FOUND("The user is not found"),
	EMPTY_SEARCH_KEY("The search key is empty"),
	INVALID_CATEGORY("The category is invalid"), 
	LATE_CANCELLATION("A booked class cannot be cancelled within an hour before its start time."), 
	UNAUTHORIZED_CANCELLATION("The cancellation of this class is forbidden."), 
	UNAUTHORIZED_COMPLETION("The completion of this class is forbidden."),
	UNAUTHORIZED_RESERVATION("The reservation of this class is forbidden. Use your student account."), 
	RESERVATION_NOT_FOUND("The reservation is not found."),
	ALREADY_BOOKED("You have already reserved this class.");
	
	private final String message;
	
	ErrorMessages(final String message){
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
