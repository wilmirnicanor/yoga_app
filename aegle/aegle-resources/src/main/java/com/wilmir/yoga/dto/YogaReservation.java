package com.wilmir.yoga.dto;

import java.io.Serializable;

public class YogaReservation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8520440409656424790L;

	private int yogaClassId;
	
	private int studentId;

	public YogaReservation(final int yogaClassId, final int studentId) {
		this.yogaClassId = yogaClassId;
		this.studentId = studentId;
	}

	public YogaReservation() {}
	
	public int getYogaClassId() {
		return yogaClassId;
	}

	public int getStudentId() {
		return studentId;
	}

}
