package com.wilmir.yoga.dto;

import java.io.Serializable;

import com.wilmir.yoga.model.User;

public class AuthenticationDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7808877091065585002L;

	private User user;
	
	private String token;

	
	public AuthenticationDTO() {}
	
	public AuthenticationDTO(final User user) {
		this.user = user;
	}
	
	public User getUser() {
		return user;
	}

	public String getToken() {
		return token;
	}

	public void setToken(final String token) {
		this.token = token;
	}
	
}
