package com.wilmir.yoga.resources;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wilmir.yoga.common.exception.YogaClassNotFoundException;
import com.wilmir.yoga.error.ErrorMessage;
import com.wilmir.yoga.error.ErrorMessages;
import com.wilmir.yoga.model.YogaClass;
import com.wilmir.yoga.services.YogaClassService;

@Path("/yoga-classes")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
public class YogaClassResource {

	@Inject
	YogaClassService yogaClassService;
	
	@GET
	public Response getAllYogaClass() {
		final Collection<YogaClass> yogaClasses = yogaClassService.findAllClasses();		
		return Response.status(200).entity(yogaClasses).build();
	}
	
	@Path("/{id}")
	@GET
	public Response getYogaClass(@PathParam("id") final int yogaClassId) {
		try {
			final YogaClass yogaClass = yogaClassService.findById(yogaClassId);
			return Response.status(200).entity(yogaClass).build();
		}catch(YogaClassNotFoundException exception) {
			return Response.status(404).entity(new ErrorMessage(ErrorMessages.YOGA_CLASS_NOT_FOUND.getMessage())).build();
		}
	}
	
	@GET
	@Path("/search")
	public Response searchYogaClasses(@QueryParam("searchKey") final String searchKey) {
		if(searchKey == null || searchKey.isEmpty()) {
			return Response.status(404).entity(new ErrorMessage(ErrorMessages.EMPTY_SEARCH_KEY.getMessage())).build();
		}
		final Collection<YogaClass> yogaClasses = yogaClassService.searchYogaClasses(searchKey);
		return Response.status(200).entity(yogaClasses).build();
	}
	
	
	@GET
	@Path("/category/{category}")
	public Response getCategory(@PathParam("category") final String category) {
		if(category == null || category.trim().isEmpty()) {
			return Response.status(404).entity(new ErrorMessage(ErrorMessages.INVALID_CATEGORY.getMessage())).build();
		}
		final Collection<YogaClass> yogaClasses = yogaClassService.getCategory(category.trim());
		return Response.status(200).entity(yogaClasses).build();
	}
}
