package com.wilmir.yoga.dto;

import java.io.Serializable;

public class Credentials implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9102765146441660224L;
	
	private String email;
	
	private String password;

	
	public Credentials() {}

	public Credentials(final String email, final String password) {
		this.email = email;
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

}
