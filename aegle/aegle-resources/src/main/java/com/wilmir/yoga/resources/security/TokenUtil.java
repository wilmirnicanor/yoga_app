package com.wilmir.yoga.resources.security;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Singleton;

import com.wilmir.yoga.dto.AuthenticationDTO;
import com.wilmir.yoga.model.User;

@Singleton
public class TokenUtil {

	private static final Map<String, User> TOKEN_HOLDER = new ConcurrentHashMap<>();

	public static AuthenticationDTO issueNewToken(final AuthenticationDTO authenticatedUser) {
		final String newToken = UUID.randomUUID().toString();
		authenticatedUser.setToken(newToken);
		TOKEN_HOLDER.put(newToken, authenticatedUser.getUser());
		return authenticatedUser;
	}

	public static boolean validateToken(final String token) {
		System.out.println("VALIDATING " + TOKEN_HOLDER);
		String tokenId = token;
		if (token.startsWith("Bearer ")) {
			tokenId = token.substring(7, token.length());
		}
		return TOKEN_HOLDER.get(tokenId) != null;
	}

	public static void revokeToken(final String token) {
		TOKEN_HOLDER.remove(token);
	}
}