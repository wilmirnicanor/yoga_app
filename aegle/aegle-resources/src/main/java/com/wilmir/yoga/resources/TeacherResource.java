package com.wilmir.yoga.resources;

import java.time.LocalDateTime;
import java.util.Collection;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wilmir.yoga.common.exception.FieldNotValidException;
import com.wilmir.yoga.common.exception.LateCancellationException;
import com.wilmir.yoga.common.exception.OverlappingYogaDateTimeException;
import com.wilmir.yoga.common.exception.UnauthorizedCancellationException;
import com.wilmir.yoga.common.exception.UnauthorizedCompletionException;
import com.wilmir.yoga.common.exception.UserNotFoundException;
import com.wilmir.yoga.common.exception.YogaClassNotFoundException;
import com.wilmir.yoga.dto.CreateYogaDTO;
import com.wilmir.yoga.error.ErrorMessage;
import com.wilmir.yoga.error.ErrorMessages;
import com.wilmir.yoga.model.Secured;
import com.wilmir.yoga.model.YogaClass;
import com.wilmir.yoga.services.YogaClassService;

@Path("/teachers")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
public class TeacherResource {

	@Inject
	YogaClassService yogaClassService;
	
	
	@POST
	@Path("/{id}/yoga-classes")
	@Secured
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response createYogaClass(@PathParam("id") final int teacherId, final CreateYogaDTO yogaDTO) {
		final YogaClass yogaClass = yogaDTO.getYogaClass();
		yogaClass.setStatus("New");
		yogaClass.setCreatedTime(LocalDateTime.now());						
		try {
			yogaClassService.createClass(yogaClass, teacherId);
			return Response.status(200).build();
		}catch(OverlappingYogaDateTimeException exception) {
			return Response.status(409).entity(new ErrorMessage(ErrorMessages.OVERLAPPING_SCHEDULE.getMessage())).build();
		}catch(UserNotFoundException exception) {
			return Response.status(404).entity(new ErrorMessage(ErrorMessages.USER_NOT_FOUND.getMessage())).build();
		}catch(FieldNotValidException exception) {
			return Response.status(403).entity(new ErrorMessage(exception.getMessage())).build();
		}
	}
	
	@GET
	@Path("/{id}/yoga-classes")
	@Secured
	public Response getAllYogaClasses(@PathParam("id") final int teacherId) {
		try {
			final Collection<YogaClass> yogaClasses = yogaClassService.findAllClassesByTeacherID(teacherId);
			return Response.status(200).entity(yogaClasses).build();
		}catch(UserNotFoundException exception) {
			return Response.status(404).entity(new ErrorMessage(ErrorMessages.USER_NOT_FOUND.getMessage())).build();
		}
	}

	
	@Path("/{id}/yoga-classes/{yoga-id}/cancellations")
	@GET
	@Secured
	public Response cancelYogaClass(@PathParam("id") final int teacherId,
			@PathParam("yoga-id") final int yogaClassId) {
		try {
			final YogaClass yogaClass = yogaClassService.cancelYogaClassByTeacher(yogaClassId, teacherId);
			return Response.status(200).entity(yogaClass).build();
		}catch(YogaClassNotFoundException exception) {
			return Response.status(404).entity(new ErrorMessage(ErrorMessages.YOGA_CLASS_NOT_FOUND.getMessage())).build();
		}catch(LateCancellationException exception) {
			return Response.status(403).entity(new ErrorMessage(ErrorMessages.LATE_CANCELLATION.getMessage())).build();
		}catch(UnauthorizedCancellationException exception) {
			return Response.status(403).entity(new ErrorMessage(ErrorMessages.UNAUTHORIZED_CANCELLATION.getMessage())).build();
		}
	}
	
	@Path("/{id}/yoga-classes/{yoga-id}/completions")
	@GET
	@Secured
	public Response completeYogaClass(@PathParam("id") final int teacherId,
			@PathParam("yoga-id") final int yogaClassId) {
		try {
			final YogaClass yogaClass = yogaClassService.completeYogaClassByTeacher(yogaClassId, teacherId);
			return Response.status(200).entity(yogaClass).build();
		}catch(YogaClassNotFoundException exception) {
			return Response.status(404).entity(new ErrorMessage(ErrorMessages.YOGA_CLASS_NOT_FOUND.getMessage())).build();
		}catch(UnauthorizedCompletionException exception) {
			return Response.status(403).entity(new ErrorMessage(ErrorMessages.UNAUTHORIZED_COMPLETION.getMessage())).build();
		}
	}
}
