const loginUrl = "http://ec2-54-76-1-191.eu-west-1.compute.amazonaws.com:8080/yoga/api/auth";

const loginCredentialsToJSON = function(){
    let userCredentials = {
        "email":$("#headerLoginEmail").val(),
        "password":$("#headerLoginPassword").val()
    }
    return JSON.stringify(userCredentials);
};

const initLogin = function(){
    $(".logged-in").show();
    $(".logged-out").hide();

    // Show container depending on user
    if(sessionStorage.getItem("aegle-user-type")==="teacher"){
        $(".teacher").show();
        $("#manage-classes").show();
        queryAllTeacherUpcomingClasses();
        $("#my-class-history").hide();
        $("#main-container").hide();
        $("#new-class").hide();
        $(".student").hide();
    }else{
        $(".student").show();
        $("#main-container").show();
        $(".teacher").hide();
    }

    // clear and hide login form
    $("#headerLoginEmail").val("");
    $("#headerLoginPassword").val("");
    $("#headerLoginForm").removeClass("d-sm-block");
    $("#headerLoginForm").removeClass("d-sm-block");
    
    // show sidenav
    $("body").toggleClass("sidenav-toggled")

    // display user with session token
    $(".aegle-user-name").text(sessionStorage.getItem("aegle-user-name"));
    $("#user-dropdown-email").text(sessionStorage.getItem("aegle-user-email"))
    $(".aegle-user-image").attr('src',`assets/img/profiles/${sessionStorage.getItem("aegle-user-image")}`);
   
}

const initLogout = function(){
    $(".logged-in").hide();
    $(".logged-out").show();
    $("#headerLoginForm").addClass("d-sm-block");
    if(!$("body").hasClass("sidenav-toggled")){
        $("body").toggleClass("sidenav-toggled")
    }

    $(".aegle-user-name").text("");
    $("#user-dropdown-email").text("");
    $("#upcomingClasses").empty();

    $("#main-container").show();  
    queryAllYogaClasses();
}

const setSessionStorage = function(loginResponse) {
    sessionStorage.setItem("aegle-token", loginResponse.token);
    sessionStorage.setItem("aegle-user-id", loginResponse.user.id);
    sessionStorage.setItem("aegle-user-email", loginResponse.user.email);
    sessionStorage.setItem("aegle-user-name", loginResponse.user.name);
    sessionStorage.setItem("aegle-user-image", loginResponse.user.image);
    sessionStorage.setItem("aegle-user-type", loginResponse.user.userType);
}

const clearSesssionStorage = function() {
    sessionStorage.removeItem("aegle-token");
    sessionStorage.removeItem("aegle-user-id");
    sessionStorage.removeItem("aegle-user-email");
    sessionStorage.removeItem("aegle-user-name");
    sessionStorage.removeItem("aegle-user-image");
    sessionStorage.removeItem("aegle-user-type");
}

const logIn = function(event){
    event.preventDefault();
    $.ajax({
        type: 'POST',
        contentType:'application/json',
        url:loginUrl + "/login",
        data: loginCredentialsToJSON(),
        success: function(loginResponse, textStatus, jqXHR){
            toastr.success("You are now logged in.");
            setSessionStorage(loginResponse);
            initLogin();
        },
        error: function(jqXHR, textStatus, errorThrown){
            toastr.error("The user is not recognized.");
        }
    })
};

const logOut = function(event){
    event.preventDefault();
    $.ajax({
        type: 'GET',
        contentType:'application/json',
        url:`${loginUrl}/logout/${sessionStorage.getItem("aegle-token")}`,
        success: function(data, textStatus, jqXHR){
            initLogout();
            clearSesssionStorage();
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log("Not logged out");
        }
    })
}


