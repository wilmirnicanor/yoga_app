const readImage = function(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageResult').attr('src', e.target.result);

            $('.image-area').show();
        };
        reader.readAsDataURL(input.files[0]);
    }
}
