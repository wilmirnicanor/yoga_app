const rootURL = "http://ec2-54-76-1-191.eu-west-1.compute.amazonaws.com:8080/yoga/api";

const setAuthHeader = function(xhr){
    let  token = 'Bearer ' + sessionStorage.getItem("aegle-token")
    xhr.setRequestHeader('Authorization', token);
}

const formToJSON = function(){
    var yogaClassObject = {
        "yogaClass":{
            "yogaType": $("#selectedYogaType").val(),
            "capacity": $("#selectedClassCapacity").val(),
            "description": $("#inputtedClassDescription").val(),
            "location": $("#inputtedLocation").val(),
            "startTime": $("#inputtedStartTime").val(),
            "endTime":  $("#inputtedEndTime").val(),
            "imageBase64Encoding": $("#imageResult").attr("src")
        }
    }
    return JSON.stringify(yogaClassObject);
}

const showAlert = function(alertElement){
    $(alertElement).fadeTo(3000, 500);
    
    $(alertElement).slideUp(500, function(){
        $(alertElement).slideUp(500);
    });
}

const alreadyBooked = function(email, students){
    return students.find(student => student.email.toLowerCase() == email.toLowerCase());
}

function getTimeString(startTime) {
    let hoursStart = startTime.getHours() <= 12 ? startTime.getHours() : startTime.getHours() - 12;
    let amOrPm = startTime.getHours() <= 12 ? "AM" : "PM";
    let startString = `${hoursStart}:${startTime.getMinutes()}${startTime.getMinutes() == 0 ? "0" : ""} ${amOrPm}`;
    return startString;
}

const updateTeacherSidebar = function(menuItem) {
    $(".sidenav-menu .teacher").each(function (index, element) {
        let target = $(element).data("target");
        if (menuItem == element) {
            $(`#${target}`).show();
            $(element).addClass("activeTeacherSidebarMenu");
            if (target == "main-container") {
                queryAllYogaClasses();
            } else if (target == "my-class-history") {
                queryAllTeacherClassHistory();
            } else if (target == "manage-classes") {
                queryAllTeacherUpcomingClasses();
            }
        } else {
            $(`#${target}`).hide();
            $(element).removeClass("activeTeacherSidebarMenu");
        }
    });
}

const updateUpcomingClassesSidebar = function(target) {
    $("#teacherUpcomingClassList a").each(function (event) {
        if (target == this) {
            $(this).addClass("teacherCurrentViewUpcomingClass");
            $(this).find("img").removeClass("grayScaleImage");

        } else {
            $(this).removeClass("teacherCurrentViewUpcomingClass");
            $(this).find("img").addClass("grayScaleImage");
        }
    });
}

const getBadgeStatus = function(status) {
    let badge="";
    switch (status.toLowerCase()) {
        case "new":
            badge = "badge-success";
            break;
        case "ready":
            badge = "badge-primary";
            break;
        case "open":
            badge = "badge-secondary";
            break;
        case "full":
            badge = "badge-pink";
            break;
        case "expired":
            badge = "badge-warning";
            break;
        case "cancelled":
            badge = "badge-dark";
            break;
        default:
            badge = "badge-dark";
            break;
    }
    return badge;
}

const initCreateAClassForm = function() {
    $('.image-area').hide();
    $("#successful-yoga-create").hide();
    $("#failed-yoga-create").hide();
    $("#inputtedStartTime").on('change', function (event) {
        $("#inputtedEndTime").attr("min", $(this).val());
    });
    $("#inputtedEndTime").on('change', function (event) {
        $("#inputtedStartTime").attr("max", $(this).val());
    });
}

const addYogaClass = function(){
    $.ajax({
        type: 'POST',
        contentType:'application/json',
        url:rootURL + "/teachers/" + sessionStorage.getItem("aegle-user-id") + "/yoga-classes",
        data: formToJSON(),
        beforeSend: setAuthHeader,
        success: function(data, textStatus, jqXHR){
            showAlert("#successful-yoga-create");
            $('#instructorActivityTableBody').trigger("reset");
            // $('#yogaClassForm')[0].reset();
        },
        error: function(jqXHR, textStatus, errorThrown){
            $("#failed-yoga-create").empty();
            $("#failed-yoga-create").append(jqXHR.responseJSON.errorMessage);
            showAlert("#failed-yoga-create");
        }
    })
}


const displayAllYogaClasses = function(allYogaClasses){
    $("#all-yoga-classes").empty();
    $(allYogaClasses).each(function(index, yogaClass){
        let badge = getBadgeStatus(yogaClass.status);
        let startTime = new Date(yogaClass.startTime);
        let endTime = new Date(yogaClass.endTime);
        let duration = ((endTime - startTime)/60000); 
        let yogaClassHTML = `<div class=\"col-lg-3 col-md-6 col-sm-12 my-2\">
                                <a class=\"yogaClassCard\" data-id=\"${yogaClass.id}\" href="#">
                                    <div class=\"card h-100\">
                                        <div class =\"yogaClassImageWrapper\">
                                            <div class = \"badgeWrapper\">
                                                <span class = \"badge ${badge}\">${yogaClass.status}</span>
                                            </div>
                                            <div class = \"yogaClassInstructorDetails d-flex flex-row align-items-center\">
                                                <img class=\"card-img-top yogaClassInstructorImage\" src=\"assets/img/profiles/${yogaClass.teacher.image}\">
                                                <div class="\yogaClassTeacherName ml-2\">Hosted by ${yogaClass.teacher.name}</div>
                                            </div>
                                            <div class =\"yogaClassImageClipper\">
                                                <img class=\"card-img-top yogaClassImage\" src=\"${yogaClass.imageBase64Encoding}\">
                                            </div>
                                        </div>
                                        <div class=\"card-body\">
                                            <h5 class=\"d-flex flex-row justify-content-between card-title\">
                                                <div>${yogaClass.yogaType} in ${yogaClass.location}
                                                </div>
                                            </h5>
                                            <div class=\"d-flex flex-row justify-content-between\">
                                                <div class=\"card-text yogaClassCardDescription\"><b>${duration} mins</b></div>
                                                <div class=\"card-text yogaClassCardDescription\">${startTime.toDateString()}</div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>`;
        $("#all-yoga-classes").append(yogaClassHTML);
    });
}

const queryAllYogaClasses = function(){
    $.ajax({
        type: 'GET',
        contentType:'application/json',
        url:rootURL +  "/yoga-classes",
        success: function(allYogaClasses, textStatus, jqXHR){
            $("#currentContainerTitle").text("Free Yoga Classes");
            $("#viewAllBtn").hide();
            displayAllYogaClasses(allYogaClasses);
        },
        error: function(jqXHR, textStatus, errorThrown){
            toastr.error(`${textStatus} There is an error in retrieving all classes`);
        }
    })
}

const displayAllCategories = function(){
    let categories = [{
        "title":"Vinyasa",
        "description":"Synchronize movement with breath"
    },
    {
        "title":"Hatha",
        "description":"Strengthen your yoga foundation"
    },
    {
        "title":"Bikram",
        "description":"Warm up muscles and prevent injury"
    }];

    $("#all-categories").empty();

    $(categories).each(function(index, category){
        let categoryCard = `<div class=\"col-sm-12 col-md-4 my-2\">
                                <a class=\"yogaCategoryLink\" data-id=\"${category.title}\" href="#">
                                    <div class=\"card categoryCard\">
                                        <img class=\"card-img categoriesImage\" src=\"assets/img/categories/${category.title.toLowerCase()}.jpeg" alt=\"${category.title}\">
                                        <div class=\"card-img-overlay\">
                                            <h5 class=\"card-title categoriesTitle\">${category.title}</h5>
                                            <p class=\"card-text\ categoriesDescription">${category.description}.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>`;        
        $("#all-categories").append(categoryCard);
    })
};


const displayAllStudentClasses = function(allStudentClasses){
    $("#upcomingClasses").empty();
    $(allStudentClasses).each(function(index, yogaClass){
        if(new Date().getTime() < new Date(yogaClass.startTime).getTime()){
            let startTime = new Date(yogaClass.startTime);
            let endTime = new Date(yogaClass.endTime);
            let durationMin = (endTime.getTime() - startTime.getTime())/60000;
            let startString = getTimeString(startTime);
            let yogaClassHTML = `<a class=\"nav-link yogaClassCard\" data-id=\"${yogaClass.id}\" href="#">
                                        <div class=\"card h-100 studentClassCard\">
                                            <div class=\"card-body\">
                                                <div class=\"studentClassCardTitle\">
                                                    <div>${yogaClass.yogaType} Class</span></div>
                                                </div>
                                                <div class=\"studentClassCardLocation\">
                                                    <div>${yogaClass.location}</div>
                                                </div>
                                                <div class=\"d-flex flex-row justify-content-between studentClassCardDateTime\">
                                                    <div class=\"card-text studentClassCardTime\">${startString}</div>
                                                    <div class=\"card-text\">${startTime.toDateString()}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>`;
            $("#upcomingClasses").append(yogaClassHTML);
        }
    });
}

const displayAYogaClass = function(yogaClass){
    $("#yogaClassModalTitle").text(`Free ${yogaClass.yogaType} Class for ${yogaClass.capacity} people in ${yogaClass.location}`)
    $("#yogaClassDescription").text(yogaClass.description);
    $("#yogaClassImage").attr("src", yogaClass.imageBase64Encoding);
    $("#yogaClassLocation").text(yogaClass.location);
   
    let startTime = new Date(yogaClass.startTime);
    let endTime = new Date(yogaClass.endTime);
    let durationMin = (endTime.getTime() - startTime.getTime())/60000;
    let startTimeString = getTimeString(startTime);

    $("#yogaClassDuration").text(durationMin + " mins")
    $("#yogaClassTime").text(`${startTimeString}`);
    $("#yogaClassDate").text(new Date(yogaClass.startTime).toDateString());
    $("#yogaClassTeacherName").text(yogaClass.teacher.name);
    $("#yogaClassTeacherImage").attr("src",`assets/img/profiles/${yogaClass.teacher.image}`);

    const studentCount = yogaClass.students.length;
    $("#yogaClassStudentsCount").text(`${studentCount>0?studentCount + " other participants of the ":""}`)
    let classUtil = (studentCount/yogaClass.capacity) * 100
    $("#classCapacityStatus").css("width",classUtil + "%");
    if(classUtil > 25){
        $("#classCapacityStatus").text(studentCount + " participants");
    }
    let remainingSlots = yogaClass.capacity - studentCount;
    
    if(sessionStorage.getItem("aegle-token")){
        if(alreadyBooked(sessionStorage.getItem("aegle-user-email"), yogaClass.students)){
            $("#yogaClassRemainingSlots").text("You have reserved this class");
            $("#cancelYogaClassBtn").show();
            $("#cancelYogaClassBtn").data("id", yogaClass.id);
            $("#reserveYogaClassBtn").hide();
        }else{
            $("#yogaClassRemainingSlots").text(`${(remainingSlots > 0 && remainingSlots  < 5) ? "Only ": ""} ${remainingSlots} slot${remainingSlots > 1 ? "s" : ""} left`);
            $("#reserveYogaClassBtn").show();
            $("#reserveYogaClassBtn").data("id", yogaClass.id);
            $("#cancelYogaClassBtn").hide();
        }
    }else{
        $("#yogaClassRemainingSlots").text(`${(remainingSlots > 0 && remainingSlots  < 5) ? "Only ": ""} ${remainingSlots} slot${remainingSlots > 1 ? "s" : ""} left`);
        $("#reserveYogaClassBtn").hide();
        $("#cancelYogaClassBtn").hide();
    }

    let badge = getBadgeStatus(yogaClass.status);
    let lastClassOfBadge = $("#yogaClassStatus").attr("class").split(" ").pop();
    if(lastClassOfBadge.startsWith("badge-")){
        $("#yogaClassStatus").removeClass(lastClassOfBadge);
    }
    $("#yogaClassStatus").addClass(badge);
    $("#yogaClassStatus").text(yogaClass.status);
    $("#yogaClassModal").modal("show");
}

const queryYogaClass = function(yogaId){
    $.ajax({
        type: 'GET',
        contentType:'application/json',
        url:rootURL + "/yoga-classes/" + yogaId,
        success: function(yogaClass, textStatus, jqXHR){
            displayAYogaClass(yogaClass);
        },
        error: function(jqXHR, textStatus, errorThrown){
            toastr.error(`${textStatus} There is an error in retrieving querying the yoga class`);
        }
    })
}

const queryCategory = function(category){
    $.ajax({
        type: 'GET',
        contentType:'application/json',
        url:rootURL + "/yoga-classes/category/" + category,
        success: function(yogaClasses, textStatus, jqXHR){
            let title = yogaClasses.length > 0 ? `Found ${yogaClasses.length} class${yogaClasses.length > 1 ? "es" : ""} for  ${category}` : `No result found for ${category}`;
            $("#currentContainerTitle").text(title);
            $("#viewAllBtn").show();
            displayAllYogaClasses(yogaClasses);
        },
        error: function(jqXHR, textStatus, errorThrown){
            toastr.error(`${textStatus} There is an error in searching the category`);
        }
    })
}

const queryAllTeacherClassHistory = function(){
    $.ajax({
        type: 'GET',
        dataType:'json',
        url:rootURL + "/teachers/" + sessionStorage.getItem("aegle-user-id") + "/yoga-classes",
        beforeSend: setAuthHeader,
        success: function(data, textStatus, jqXHR){
            const table = $('#instructorActivityTable').DataTable();
            table.clear();
            data.forEach(yogaClass => {
                const startTime = new Date(yogaClass.startTime);
                const endTime = new Date(yogaClass.endTime);
                const dateString = startTime.toLocaleDateString('en-GB');
                let hours = startTime.getHours() <= 12 ? startTime.getHours() : startTime.getHours() - 12;
                let amOrPm = startTime.getHours() <= 12 ? "AM" : "PM";
                const hourMin = `${hours}:${startTime.getMinutes()}${startTime.getMinutes()==0?"0":""} ${amOrPm}`;
                const duration = (endTime - startTime)/60000;
                const description = `${yogaClass.yogaType} in ${yogaClass.location}`;
                const badge = getBadgeStatus(yogaClass.status);
                const status = `<span class = \"badge ${badge}\">${yogaClass.status}</span>`;
                table.row.add([dateString, hourMin, duration, description, yogaClass.capacity, yogaClass.students.length, status]);
            });
            table.draw();
            feather.replace();

        },
        error: function(jqXHR, textStatus, errorThrown){
            toastr.error("Sorry! " + textStatus);
        }
    });
}

const displayTeacherUpcomingClasses = function(futureClasses){
    $("#teacherUpcomingClassList").empty();
    let firstUpcomingClass;
    
    let upcomingClasses = futureClasses.filter(yogaClass => {
        return (new Date(yogaClass.startTime) > new Date() && yogaClass.status != "Cancelled")  || yogaClass.status == "Expired";
    });

    if(upcomingClasses.length > 0){
        $("#teacherYogaClassDetail").show();
    }else{
        $("#teacherYogaClassDetail").hide();
    }

    $(upcomingClasses).each(function(index, yogaClass){
        if(!firstUpcomingClass){
            firstUpcomingClass = yogaClass;
            displayUpcomingClassForTeacher(firstUpcomingClass);
        }
        let startTime = new Date(yogaClass.startTime);
        let endTime = new Date(yogaClass.endTime);
        let startString = getTimeString(startTime);
        let endString = getTimeString(endTime);
        let yogaClassCard = `<div class=\"row p-2\">
                                <a class=\"futureTeacherClassLink\" data-id=\"${yogaClass.id}\" href="#">
                                    <div class=\"card categoryCard\">
                                        <img class=\"card-img categoriesImage grayScaleImage\" src=\"${yogaClass.imageBase64Encoding}\">
                                        <div class=\"card-img-overlay\">
                                            <h5 class=\"card-title categoriesTitle\">${yogaClass.yogaType}</h5>
                                            <div class=\"teacherClassCardLocation\">
                                                <div>${yogaClass.location}</div>
                                            </div>
                                            <div class=\"d-flex flex-column teacherClassCardDateTime\">
                                                <div class=\"card-text teacherClassCardTime\">${startString} to ${endString}</div>
                                                <div class=\"card-text\">${startTime.toDateString()}</div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>`;
        $("#teacherUpcomingClassList").append(yogaClassCard);
    });
}

const queryAllTeacherUpcomingClasses = function(){
    $.ajax({
        type: 'GET',
        dataType:'json',
        url:rootURL + "/teachers/" + sessionStorage.getItem("aegle-user-id") + "/yoga-classes",
        beforeSend: setAuthHeader,
        success: function(yogaClasses, textStatus, jqXHR){
            displayTeacherUpcomingClasses(yogaClasses);
        },
        error: function(jqXHR, textStatus, errorThrown){
            toastr.error(`Failed to fetch the classes ${textStatus}`);
        }
    });
}


const displayStudents = function(students){
    if(students.length > 0){
        $("#classStudentImagesContainer").show();
        $("#classStudentImagesContainerTitle").show();
        $("#classStudentImages").empty();
    }else{
        $("#classStudentImagesContainerTitle").hide();
        $("#classStudentImagesContainer").hide();
    }

    $(students).each(function(index, student){
        let studentImage = `<div class=\"m-2\" data-toggle=\"tooltip\" data-placement=\"bottom\" data-original-title=\"${student.name}\">
                                <img class=\"studentImageOnCard\" src=\"assets/img/profiles/${student.image}\">
                            </div>`;
        $("#classStudentImages").append(studentImage);
    });
    $('[data-toggle="tooltip"]').tooltip();
}


const displayUpcomingClassForTeacher = function(yogaClass){
    $("#teacherYogaClassDetailTitle").text(`${yogaClass.yogaType} Class for ${yogaClass.capacity} student${yogaClass.capacity>1?"s":""} in ${yogaClass.location}`);
    $("#teacherYogaClassImage").attr("src",yogaClass.imageBase64Encoding);
    let startTime = new Date(yogaClass.startTime);
    let endTime = new Date(yogaClass.endTime);
    let durationMin = (endTime.getTime() - startTime.getTime())/60000;
    $("#teacherYogaClassDetailTime").text(`${getTimeString(startTime)} to ${getTimeString(endTime)}`);
    $("#teacherYogaClassDetailDate").text(startTime.toDateString());
    $("#teacherYogaClassDetailDuration").text(`${durationMin} mins`);
    $("#teacherYogaClassDetailDescription").text(yogaClass.description);
    let badge = getBadgeStatus(yogaClass.status);
    let lastClassOfBadge = $("#teacherYogaClassStatus").attr("class").split(" ").pop();
    if(lastClassOfBadge.startsWith("badge-")){
        $("#teacherYogaClassStatus").removeClass(lastClassOfBadge);
    }
    $("#teacherYogaClassStatus").addClass(badge);
    $("#teacherYogaClassStatus").text(yogaClass.status);
    const studentCount = yogaClass.students.length;
    let classUtil = (studentCount/yogaClass.capacity) * 100
    $("#teacherClassCapacityStatus").css("width",classUtil + "%");
    if(classUtil > 25){
        $("#teacherClassCapacityStatus").text(studentCount + " participants");
    }

    console.log(yogaClass);
    displayStudents(yogaClass.students);

    if(new Date() > new Date(yogaClass.endTime)){
        $("#manage-classes #markCompleteYogaClassBtn").show();
        $("#manage-classes #teacherCancelYogaClassBtn").hide();
        $("#manage-classes #markCompleteYogaClassBtn").data("id", yogaClass.id);
    }else{
        $("#manage-classes #markCompleteYogaClassBtn").hide();
        $("#manage-classes #teacherCancelYogaClassBtn").show();
    }

    $("#manage-classes #teacherCancelYogaClassBtn").data("id", yogaClass.id);
}

const queryUpcomingClassForTeacher = function(yogaId){
    $.ajax({
        type: 'GET',
        contentType:'application/json',
        url:rootURL + "/yoga-classes/" + yogaId,
        success: function(yogaClass, textStatus, jqXHR){
            displayUpcomingClassForTeacher(yogaClass);
        },
        error: function(jqXHR, textStatus, errorThrown){
            toastr.error("Sorry! There is an error in querying upcoming class for teacher");
        }
    })
}

const updateClassStatusByTeacher = function(yogaClassId, status){
    console.log(rootURL + "/yoga-classes/" + yogaClassId + "/" + status);
    $.ajax({
        type: 'GET',
        contentType:'application/json',
        url:rootURL + "/teachers/" + sessionStorage.getItem("aegle-user-id") + "/yoga-classes/" + yogaClassId + "/" + status,
        beforeSend:setAuthHeader,
        success: function(yogaClass, textStatus, jqXHR){
            queryAllTeacherUpcomingClasses();
            if(status == "cancellations"){
                toastr.success("This class is now canceled");
            }else{
                toastr.success("This class is now marked as complete");
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            toastr.error("Sorry! " + jqXHR.responseJSON.errorMessage);
        }
    })
}

const initializeToaster = function(){
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
}

const main = function(){
    initializeBootstrapPlugIns(jQuery);
    initializeToaster();

    // View All yoga classes
    queryAllYogaClasses();

    // Display categories
    displayAllCategories();

    // Check Session Storage
    if(sessionStorage.getItem("aegle-token")){
        initLogin();
    }else{
        initLogout();
    }

    // Log in
    $("#headerLogin").click(logIn)
    $("#headerLogout").click(logOut);
    $("#headerLoginPassword").keypress(function(event){
        if(event.which == 13 && $("#headerLoginEmail").val()){
            $("#headerLogin").click();
            return false;
        }
    });

    // View a yoga class
    $("#all-yoga-classes").on("click","a", function(event){
        event.preventDefault();
        queryYogaClass($(this).data("id"))
    });

    // Reset modal on close
    $('#yogaClassModal').on('hidden.bs.modal', function () {
        if($("#currentContainerTitle").text() == "Free Yoga Classes"){
            queryAllYogaClasses();
        }
        $("#classCapacityStatus").text("");
    })

   // View a yoga class
   $("#all-categories").on("click","a", function(event){
        event.preventDefault();
        let category = $(this).data("id");
        queryCategory(category);
        $('#yogaClassesSearch').val(category);

   });

   // View an upcoming yoga session for teacher
   $("#teacherUpcomingClassList").on("click", "a", function(event){
        event.preventDefault();
        let target = this;
        queryUpcomingClassForTeacher($(target).data("id"))
        updateUpcomingClassesSidebar(target);
   });

    // View All Classes
    $("#viewAllBtn").click(function(event){
        queryAllYogaClasses();
        return false;
    });

    // Teachers Side Navigation
    $(".sidenav-menu").on("click", ".teacher", function(event){
        let menuItem = this;
        updateTeacherSidebar(menuItem);
    });

    // Initialize Create Class Form
    initCreateAClassForm();

    // Create A Yoga Class
    $('#yogaClassForm').on("submit", function(event) {
        event.preventDefault();
		addYogaClass();
	});

    // Upload an Image
    $('#uploadedImage').on('change', function(){
        readImage(this);
    });

    // Mark Class As Complete
    $("#manage-classes").on("click", "#markCompleteYogaClassBtn", function(event){
        updateClassStatusByTeacher($(this).data("id"), "completions");
    });

    // Cancel A Class By Teacher
    $("#manage-classes").on("click", "#teacherCancelYogaClassBtn", function(event){
        $("#teacherCancellationModal").modal("show");
        $("#teacherCancelConfirmBtn").data("id", $(this).data("id"));
    });
    $("#teacherCancelConfirmBtn").click(function(event){
        updateClassStatusByTeacher($(this).data("id"), "cancellations");
    });

}

$(document).ready(main);


