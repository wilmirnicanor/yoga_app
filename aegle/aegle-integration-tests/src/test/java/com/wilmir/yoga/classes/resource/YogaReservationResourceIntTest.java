package com.wilmir.yoga.classes.resource;


import static org.junit.Assert.*;

import java.io.File;
import java.net.URL;
import java.time.LocalDateTime;

import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.wilmir.yoga.common.model.HttpCode;
import com.wilmir.yoga.commontests.utils.JsonReader;
import com.wilmir.yoga.commontests.utils.ResourceClient;
import com.wilmir.yoga.commontests.utils.ResourceDefinitions;

import static com.wilmir.yoga.commontests.utils.FileTestNameUtils.*;
import static com.wilmir.yoga.commontests.utils.JsonTestUtils.*;
import static com.wilmir.yoga.commontests.utils.YogaJsonGeneratorUtils.generateReservation;
import static com.wilmir.yoga.commontests.utils.YogaJsonGeneratorUtils.generateYogaClassJSONDTOWithDate;
import static com.wilmir.yoga.commontests.yogaclass.YogaClassesForTestsRepositoryUtil.vinyasa;


@RunWith(Arquillian.class)
public class YogaReservationResourceIntTest {

	@ArquillianResource
	private URL url; 

	private ResourceClient resourceClient;

	private static final String PATH_RESOURCE = ResourceDefinitions.YOGA_RESERVATIONS.getResourceName();
	private static final String TEACHERS_RESOURCE = ResourceDefinitions.TEACHERS.getResourceName();
	private static final String LOGIN_RESOURCE = ResourceDefinitions.LOGIN.getResourceName();
	private static final String LOGOUT_RESOURCE = ResourceDefinitions.LOGOUT.getResourceName();
	private String token;
	
	@Deployment
	public static WebArchive createDeployment() {
		return ShrinkWrap
				.create(WebArchive.class)
				.addPackages(true, "com.wilmir.yoga")
				.addAsResource("importForYogaReservation.sql", "META-INF/import.sql")
				.addAsResource("persistence-integration.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.setWebXML(new File("src/test/resources/web.xml"))
				.addAsLibraries(
						Maven.resolver().resolve("com.google.code.gson:gson:2.3.1", 
								"org.mockito:mockito-core:1.9.5", 
								"org.hibernate:hibernate-core:5.3.7.Final")
								.withTransitivity().asFile());
	}

	@Before
	public void initTestCase() {
		this.resourceClient = new ResourceClient(url);
		final Response loginRequest = resourceClient.resourcePath(LOGIN_RESOURCE)
				.postWithFile(getPathFileRequest(LOGIN_RESOURCE, "validCredentials.json"), "");		
		token = JsonReader.readAsJsonObject(loginRequest.readEntity(String.class))
				.get("token")
				.getAsString();
	}
	
	@After
	public void logoutUser() {
		resourceClient.resourcePath(LOGOUT_RESOURCE + "/" + token).get(token);
	}

	
	@Test
	@RunAsClient
	public void testValidReservation() {
		final Response response = resourceClient.resourcePath(PATH_RESOURCE)
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "validReservation.json"), token);		
		assertEquals(HttpCode.OK.getCode(), response.getStatus());
		assertJsonResponseWithFile(response, "validReservationResponse.json");
	}
	
	@Test
	@RunAsClient
	public void testDoubleBooking() {
		final Response response = resourceClient.resourcePath(PATH_RESOURCE)
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "doubleBooking.json"), token);	
		assertEquals(HttpCode.FORBIDDEN.getCode(), response.getStatus());
	}
	
	
	@Test
	@RunAsClient
	public void testUnauthorizedReservation() {
		final Response response = resourceClient.resourcePath(PATH_RESOURCE)
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "unauthorizedReservation.json"), token);		
		assertEquals(HttpCode.FORBIDDEN.getCode(), response.getStatus());
		assertJsonResponseWithFile(response, "invalidReservationResponseUnauthorized.json");
	}
	
	@Test
	@RunAsClient
	public void testInvalidReservationDuetoInexistentClass() {
		final Response response = resourceClient.resourcePath(PATH_RESOURCE)
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "invalidReservationInexistentClass.json"), token);
		assertEquals(HttpCode.NOT_FOUND.getCode(), response.getStatus());
		assertJsonResponseWithFile(response, "invalidReservationResponseInexistentClass.json");
		
	}
	
	@Test
	@RunAsClient
	public void testInvalidReservationDuetoInexistentStudent() {
		final Response response = resourceClient.resourcePath(PATH_RESOURCE)
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "invalidReservationInexistentStudent.json"), token);
		assertEquals(HttpCode.NOT_FOUND.getCode(), response.getStatus());
		assertJsonResponseWithFile(response, "invalidReservationResponseInexistentStudent.json");
		
	}
	
	@Test
	@RunAsClient
	public void testInvalidReservationFullClass() {
		final Response response = resourceClient.resourcePath(PATH_RESOURCE)
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "invalidReservationFullClass.json"), token);
		assertEquals(HttpCode.FORBIDDEN.getCode(), response.getStatus());
		assertJsonResponseWithFile(response, "invalidReservationResponseFullClass.json");
	}
	
	
	@Test
	@RunAsClient
	public void testInvalidReservationOngoingClass() {		
		final Response createClass = resourceClient.resourcePath(TEACHERS_RESOURCE + "/1/yoga-classes")
				.postWithContent(generateYogaClassJSONDTOWithDate(vinyasa(), 
						LocalDateTime.now().plusSeconds(5), 
						LocalDateTime.now().plusMinutes(45)),token);	
		assertEquals(HttpCode.OK.getCode(), createClass.getStatus());

		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {}
		
		final Response response = resourceClient.resourcePath(PATH_RESOURCE)
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "invalidReservationOngoingClass.json"), token);
		assertEquals(HttpCode.FORBIDDEN.getCode(), response.getStatus());
		assertJsonResponseWithFile(response, "invalidReservationResponseOngoingClass.json");
	}
	
	
	private void assertJsonResponseWithFile(final Response response, final String fileName) {
		assertJsonMatchesFileContent(response.readEntity(String.class), getPathFileResponse(PATH_RESOURCE, fileName));
	}

}
