package com.wilmir.yoga.commontests.utils;

import java.time.LocalDateTime;

import com.google.gson.JsonObject;
import com.wilmir.yoga.model.YogaClass;

public class YogaJsonGeneratorUtils {

	public static String generateYogaClassJSONDTOWithDate(YogaClass yogaClass, LocalDateTime startTime, LocalDateTime endTime) {		
		final JsonObject jsonObject =  new JsonObject();
		jsonObject.addProperty("yogaType", yogaClass.getYogaType());
		jsonObject.addProperty("capacity", yogaClass.getCapacity());
		jsonObject.addProperty("description", yogaClass.getDescription());
		jsonObject.addProperty("startTime", startTime.toString());
		jsonObject.addProperty("endTime", endTime.toString());
		jsonObject.addProperty("location", yogaClass.getLocation());
		jsonObject.addProperty("imageBase64Encoding", yogaClass.getImageBase64Encoding());
		JsonObject yogaDTO = new JsonObject();
		yogaDTO.add("yogaClass", jsonObject);
		return yogaDTO.toString();
	}
	
	public static String generateReservation(final int yogaClassId, final int studentId) {		
		final JsonObject jsonObject =  new JsonObject();
		jsonObject.addProperty("yogaClassId", yogaClassId);
		jsonObject.addProperty("studentId", studentId);
		return jsonObject.toString();
	}
}
