package com.wilmir.yoga.classes.resource;


import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.io.File;
import java.net.URL;

import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.wilmir.yoga.common.model.HttpCode;
import com.wilmir.yoga.commontests.utils.JsonReader;
import com.wilmir.yoga.commontests.utils.ResourceClient;
import com.wilmir.yoga.commontests.utils.ResourceDefinitions;

import static com.wilmir.yoga.commontests.utils.FileTestNameUtils.*;
import static com.wilmir.yoga.commontests.utils.JsonTestUtils.*;



@RunWith(Arquillian.class)
public class YogaClassSearchIntTest {

	@ArquillianResource
	private URL url; 

	private ResourceClient resourceClient;

	private static final String SEARCH_RESOURCE = ResourceDefinitions.YOGA_SEARCH.getResourceName();
	private static final String CATEGORY_RESOURCE = ResourceDefinitions.YOGA_CATEGORY.getResourceName();
	private static final String LOGIN_RESOURCE = ResourceDefinitions.LOGIN.getResourceName();
	private static final String LOGOUT_RESOURCE = ResourceDefinitions.LOGOUT.getResourceName();
	private String token;
	
	@Deployment
	public static WebArchive createDeployment() {
		return ShrinkWrap
				.create(WebArchive.class)
				.addPackages(true, "com.wilmir.yoga")
				.addAsResource("importForYogaReservation.sql", "META-INF/import.sql")
				.addAsResource("persistence-integration.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.setWebXML(new File("src/test/resources/web.xml"))
				.addAsLibraries(
						Maven.resolver().resolve("com.google.code.gson:gson:2.3.1", 
								"org.mockito:mockito-core:1.9.5", 
								"org.hibernate:hibernate-core:5.3.7.Final")
								.withTransitivity().asFile());
	}

	@Before
	public void initTestCase() {
		this.resourceClient = new ResourceClient(url);
		final Response loginRequest = resourceClient.resourcePath(LOGIN_RESOURCE)
				.postWithFile(getPathFileRequest(LOGIN_RESOURCE, "validCredentials.json"),"");
		token = JsonReader.readAsJsonObject(loginRequest.readEntity(String.class))
				.get("token")
				.getAsString();
	}
	
	@After
	public void logoutUser() {
		resourceClient.resourcePath(LOGOUT_RESOURCE + "/" + token).get(token);
	}

	
	@Test
	@RunAsClient
	public void testSearchWithResult() {
		final Response responseGet = resourceClient.resourcePath(SEARCH_RESOURCE  + "?searchKey=vinyasa").get(token);
		
		assertThat(responseGet.getStatus(), is(equalTo(HttpCode.OK.getCode())));
		
		assertJsonResponseWithFile(responseGet, "validSearchKey.json");
	}
	
	
	@Test
	@RunAsClient
	public void testSearchWithEmptyResult() {
		final Response responseGet = resourceClient.resourcePath(SEARCH_RESOURCE  + "?searchKey=ihavenothing").get(token);
				
		assertThat(responseGet.getStatus(), is(equalTo(HttpCode.OK.getCode())));
		assertJsonResponseWithFile(responseGet, "emptyResult.json");
	}
	
	
	@Test
	@RunAsClient
	public void testSearchWithNullKey() {
		final Response responseGet = resourceClient.resourcePath(SEARCH_RESOURCE  + "?searchKey=").get(token);
				
		assertThat(responseGet.getStatus(), is(equalTo(HttpCode.NOT_FOUND.getCode())));
		
		assertJsonResponseWithFile(responseGet, "nullSearchKey.json");
	}
	
	
	@Test
	@RunAsClient
	public void testGetCategoryWithResult() {
		final Response responseGet = resourceClient.resourcePath(CATEGORY_RESOURCE  + "/vinyasa").get(token);
		
		assertThat(responseGet.getStatus(), is(equalTo(HttpCode.OK.getCode())));
		
		assertJsonMatchesFileContent(responseGet.readEntity(String.class), getPathFileResponse(CATEGORY_RESOURCE, "validCategory.json"));
	}
	
	@Test
	@RunAsClient
	public void testGetNullCategory() {
		final Response responseGet = resourceClient.resourcePath(CATEGORY_RESOURCE  + "/ ").get(token);
		
		assertThat(responseGet.getStatus(), is(equalTo(HttpCode.NOT_FOUND.getCode())));
		
		assertJsonMatchesFileContent(responseGet.readEntity(String.class), getPathFileResponse(CATEGORY_RESOURCE, "nullCategory.json"));
	}
	
	private void assertJsonResponseWithFile(final Response response, final String fileName) {
		assertJsonMatchesFileContent(response.readEntity(String.class), getPathFileResponse(SEARCH_RESOURCE, fileName));
	}
	

}
