package com.wilmir.yoga.classes.resource;


import static org.junit.Assert.*;

import java.io.File;
import java.net.URL;

import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.gson.JsonObject;
import com.wilmir.yoga.common.model.HttpCode;
import com.wilmir.yoga.commontests.utils.JsonReader;
import com.wilmir.yoga.commontests.utils.ResourceClient;
import com.wilmir.yoga.commontests.utils.ResourceDefinitions;
import com.wilmir.yoga.error.ErrorMessages;

import static com.wilmir.yoga.commontests.utils.FileTestNameUtils.*;
import static com.wilmir.yoga.commontests.utils.JsonTestUtils.assertJsonMatchesFileContent;


@RunWith(Arquillian.class)
public class YogaClassResourceIntTest {

	@ArquillianResource
	private URL url; 

	private ResourceClient resourceClient;

	private static final String PATH_RESOURCE = ResourceDefinitions.YOGA_CLASSES.getResourceName();
	private static final String LOGIN_RESOURCE = ResourceDefinitions.LOGIN.getResourceName();
	private static final String LOGOUT_RESOURCE = ResourceDefinitions.LOGOUT.getResourceName();
	private String token;


	@Deployment
	public static WebArchive createDeployment() {
		return ShrinkWrap
				.create(WebArchive.class)
				.addPackages(true, "com.wilmir.yoga")
				.addAsResource("import.sql", "META-INF/import.sql")
				.addAsResource("persistence-integration.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.setWebXML(new File("src/test/resources/web.xml"))
				.addAsLibraries(
						Maven.resolver().resolve("com.google.code.gson:gson:2.3.1", 
								"org.mockito:mockito-core:1.9.5", 
								"org.hibernate:hibernate-core:5.3.7.Final")
								.withTransitivity().asFile());
	}	
	
	
	@Before
	public void initTestCase() {
		this.resourceClient = new ResourceClient(url);
		final Response loginRequest = resourceClient.resourcePath(LOGIN_RESOURCE)
				.postWithFile(getPathFileRequest(LOGIN_RESOURCE, "validCredentials.json"), "");		
		token = JsonReader.readAsJsonObject(loginRequest.readEntity(String.class))
				.get("token")
				.getAsString();		
	}
	
	
	@After
	public void logoutUser() {
		resourceClient.resourcePath(LOGOUT_RESOURCE + "/" + token).get(token);
	}
	
	
	@Test
	@RunAsClient
	public void findExistingYogaClass() {		
		final Response responseGet = resourceClient.resourcePath(PATH_RESOURCE + "/1").get(token);
		assertEquals(HttpCode.OK.getCode(), responseGet.getStatus());
		assertJsonResponseWithFile(responseGet, "existingYogaClass.json");
	}
	
	
	@Test
	@RunAsClient
	public void findAllClasses() {
		final Response responseGet = resourceClient.resourcePath(PATH_RESOURCE).get(token);
		assertEquals(HttpCode.OK.getCode(), responseGet.getStatus());
		assertJsonResponseWithFile(responseGet, "allYogaClasses.json");
	}
	
	
	@Test
	@RunAsClient
	public void findInexistentYogaClass() {		
		final Response responseGet = resourceClient.resourcePath(PATH_RESOURCE + "/999999").get(token);
		assertEquals(HttpCode.NOT_FOUND.getCode(), responseGet.getStatus());
		final JsonObject errorMessage = JsonReader.readAsJsonObject(responseGet.readEntity(String.class));
		assertEquals(ErrorMessages.YOGA_CLASS_NOT_FOUND.getMessage(), JsonReader.getStringOrNull(errorMessage, "errorMessage"));
	}
	
	
	private void assertJsonResponseWithFile(final Response response, final String fileName) {
		assertJsonMatchesFileContent(response.readEntity(String.class), getPathFileResponse(PATH_RESOURCE, fileName));
	}
}
