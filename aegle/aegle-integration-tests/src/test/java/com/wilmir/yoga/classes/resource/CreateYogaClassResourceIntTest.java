package com.wilmir.yoga.classes.resource;


import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.File;
import java.net.URL;

import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.gson.JsonObject;
import com.wilmir.yoga.common.model.HttpCode;
import com.wilmir.yoga.commontests.utils.JsonReader;
import com.wilmir.yoga.commontests.utils.ResourceClient;
import com.wilmir.yoga.commontests.utils.ResourceDefinitions;
import com.wilmir.yoga.error.ErrorMessages;

import static com.wilmir.yoga.commontests.utils.FileTestNameUtils.*;
import static com.wilmir.yoga.commontests.utils.JsonTestUtils.assertJsonMatchesFileContent;


@RunWith(Arquillian.class)
public class CreateYogaClassResourceIntTest {

	@ArquillianResource
	private URL url; 

	private ResourceClient resourceClient;

	private static final String PATH_RESOURCE = ResourceDefinitions.TEACHERS.getResourceName();
	private static final String LOGIN_RESOURCE = ResourceDefinitions.LOGIN.getResourceName();
	private static final String LOGOUT_RESOURCE = ResourceDefinitions.LOGOUT.getResourceName();
	private String token;


	@Deployment
	public static WebArchive createDeployment() {
		return ShrinkWrap
				.create(WebArchive.class)
				.addPackages(true, "com.wilmir.yoga")
				.addAsResource("import.sql", "META-INF/import.sql")
				.addAsResource("persistence-integration.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.setWebXML(new File("src/test/resources/web.xml"))
				.addAsLibraries(
						Maven.resolver().resolve("com.google.code.gson:gson:2.3.1", 
								"org.mockito:mockito-core:1.9.5", 
								"org.hibernate:hibernate-core:5.3.7.Final")
								.withTransitivity().asFile());
	}	
	
	@Before
	public void initTestCase() {
		this.resourceClient = new ResourceClient(url);
		final Response loginRequest = resourceClient.resourcePath(LOGIN_RESOURCE)
				.postWithFile(getPathFileRequest(LOGIN_RESOURCE, "validCredentials.json"), "");		
		token = JsonReader.readAsJsonObject(loginRequest.readEntity(String.class))
				.get("token")
				.getAsString();		
	}
	
	@After
	public void logoutUser() {
		resourceClient.resourcePath(LOGOUT_RESOURCE + "/" + token).get(token);
	}

	
	@Test
	@RunAsClient
	public void addNewYogaClassesForInexistentTeacher() {		
		final Response response = resourceClient.resourcePath(PATH_RESOURCE + "/999999/yoga-classes")
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "newYogaClass.json"), token);
		assertEquals(HttpCode.NOT_FOUND.getCode(), response.getStatus());
		assertJsonResponseWithFile(response, "userNotFound.json");
	}
	
	
	@Test
	@RunAsClient
	public void addNewYogaClassForExistingTeacher() {	
		final Response response = resourceClient.resourcePath(PATH_RESOURCE + "/1/yoga-classes")
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "newYogaClass.json"), token);
		
		assertEquals(HttpCode.OK.getCode(), response.getStatus());
		
		final Response responseGet = resourceClient.resourcePath(PATH_RESOURCE + "/1/yoga-classes").get(token);
		assertThat(responseGet.getStatus(), is(equalTo(HttpCode.OK.getCode())));
	}
	
	
	@Test
	@RunAsClient
	public void addNewYogaClassForExistingTeacherInvalidOldDate() {	
		final Response response = resourceClient.resourcePath(PATH_RESOURCE + "/1/yoga-classes")
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "newYogaClassInvalidDate.json"), token);
		
		assertEquals(HttpCode.FORBIDDEN.getCode(), response.getStatus());
		assertJsonResponseWithFile(response, "newYogaClassInvalidDate.json");
	}
	
	@Test
	@RunAsClient
	public void addNewYogaClassForExistingTeacherSameDate() {	
		final Response response = resourceClient.resourcePath(PATH_RESOURCE + "/1/yoga-classes")
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "newYogaClassEqualStartEndDate.json"), token);
		
		assertEquals(HttpCode.FORBIDDEN.getCode(), response.getStatus());
		assertJsonResponseWithFile(response, "newYogaClassInvalidDate.json");
	}
	
	@Test
	@RunAsClient
	public void addNewYogaClassForExistingTeacherStartAfterEndDate() {	
		final Response response = resourceClient.resourcePath(PATH_RESOURCE + "/1/yoga-classes")
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "newYogaClassStartAfterEndDate.json"), token);
		
		assertEquals(HttpCode.FORBIDDEN.getCode(), response.getStatus());
		assertJsonResponseWithFile(response, "newYogaClassInvalidDate.json");
	}

	
	@Test
	@RunAsClient
	public void testAddOverlappingClassWithDifferentDateTime() {
		final Response responseOne = resourceClient.resourcePath(PATH_RESOURCE + "/1/yoga-classes")
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "newOverlappingYogaClassOne.json"), token);
		
		assertEquals(HttpCode.OK.getCode(), responseOne.getStatus());
		
		final Response responseGet = resourceClient.resourcePath(PATH_RESOURCE + "/1/yoga-classes").get(token);
		assertThat(responseGet.getStatus(), is(equalTo(HttpCode.OK.getCode())));

		final Response responseTwo = resourceClient.resourcePath(PATH_RESOURCE + "/1/yoga-classes")
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "newOverlappingYogaClassTwo.json"), token);
		
		assertEquals(HttpCode.CONFLICT.getCode(), responseTwo.getStatus());
		
		final JsonObject errorMessage = JsonReader.readAsJsonObject(responseTwo.readEntity(String.class));
		assertEquals(ErrorMessages.OVERLAPPING_SCHEDULE.getMessage(), JsonReader.getStringOrNull(errorMessage, "errorMessage"));
	}
	
	
	private void assertJsonResponseWithFile(final Response response, final String fileName) {
		assertJsonMatchesFileContent(response.readEntity(String.class), getPathFileResponse(PATH_RESOURCE, fileName));
	}
	
}
