package com.wilmir.yoga.classes.resource;


import static org.junit.Assert.*;

import java.io.File;
import java.net.URL;

import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;


import com.wilmir.yoga.common.model.HttpCode;
import com.wilmir.yoga.commontests.utils.ResourceClient;
import com.wilmir.yoga.commontests.utils.ResourceDefinitions;

import static com.wilmir.yoga.commontests.utils.FileTestNameUtils.*;


@RunWith(Arquillian.class)
public class AuthResourceIntTest {

	@ArquillianResource
	private URL url; 

	private ResourceClient resourceClient;

	private static final String PATH_RESOURCE = ResourceDefinitions.LOGIN.getResourceName();

	@Deployment
	public static WebArchive createDeployment() {
		return ShrinkWrap
				.create(WebArchive.class)
				.addPackages(true, "com.wilmir.yoga")
				.addAsResource("import.sql", "META-INF/import.sql")
				.addAsResource("persistence-integration.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.setWebXML(new File("src/test/resources/web.xml"))
				.addAsLibraries(
						Maven.resolver().resolve("com.google.code.gson:gson:2.3.1", 
								"org.mockito:mockito-core:1.9.5", 
								"org.hibernate:hibernate-core:5.3.7.Final")
								.withTransitivity().asFile());
	}

	@Before
	public void initTestCase() {
		this.resourceClient = new ResourceClient(url);
	}

	@Test
	@RunAsClient
	public void testInvalidUser() {
		final Response response = resourceClient.resourcePath(PATH_RESOURCE)
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "invalidCredentials.json"), "");
		
		assertEquals(HttpCode.UNAUTHORIZED.getCode(), response.getStatus());
	}
	
}
