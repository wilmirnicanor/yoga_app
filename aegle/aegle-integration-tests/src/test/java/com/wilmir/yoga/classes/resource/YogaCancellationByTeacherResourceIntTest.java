package com.wilmir.yoga.classes.resource;


import static org.junit.Assert.*;

import java.io.File;
import java.net.URL;
import java.time.LocalDateTime;

import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.wilmir.yoga.common.model.HttpCode;
import com.wilmir.yoga.commontests.utils.JsonReader;
import com.wilmir.yoga.commontests.utils.ResourceClient;
import com.wilmir.yoga.commontests.utils.ResourceDefinitions;

import static com.wilmir.yoga.commontests.utils.FileTestNameUtils.*;
import static com.wilmir.yoga.commontests.utils.JsonTestUtils.*;
import static com.wilmir.yoga.commontests.utils.YogaJsonGeneratorUtils.*;
import static com.wilmir.yoga.commontests.yogaclass.YogaClassesForTestsRepositoryUtil.*;

@RunWith(Arquillian.class)
public class YogaCancellationByTeacherResourceIntTest {
 
	@ArquillianResource
	private URL url; 

	private ResourceClient resourceClient;

	private static final String TEACHERS_RESOURCE = ResourceDefinitions.TEACHERS.getResourceName();
	private static final String RESERVATION_RESOURCE = ResourceDefinitions.YOGA_RESERVATIONS.getResourceName();
	private static final String LOGIN_RESOURCE = ResourceDefinitions.LOGIN.getResourceName();
	private static final String LOGOUT_RESOURCE = ResourceDefinitions.LOGOUT.getResourceName();
	private String token;


	@Deployment
	public static WebArchive createDeployment() {
		return ShrinkWrap
				.create(WebArchive.class)
				.addPackages(true, "com.wilmir.yoga")
				.addAsResource("importForYogaReservation.sql", "META-INF/import.sql")
				.addAsResource("persistence-integration.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.setWebXML(new File("src/test/resources/web.xml"))
				.addAsLibraries(
						Maven.resolver().resolve("com.google.code.gson:gson:2.3.1", 
								"org.mockito:mockito-core:1.9.5", 
								"org.hibernate:hibernate-core:5.3.7.Final")
								.withTransitivity().asFile());
	}	
	
	@Before
	public void initTestCase() {
		this.resourceClient = new ResourceClient(url);
		final Response loginRequest = resourceClient.resourcePath(LOGIN_RESOURCE)
				.postWithFile(getPathFileRequest(LOGIN_RESOURCE, "validCredentials.json"), "");		
		token = JsonReader.readAsJsonObject(loginRequest.readEntity(String.class))
				.get("token")
				.getAsString();		
	}
	
	@After
	public void logoutUser() {
		resourceClient.resourcePath(LOGOUT_RESOURCE + "/" + token).get(token);
	}
	
	
	@Test
	@RunAsClient
	public void testLateCancellation() {		
		final Response createClass = resourceClient.resourcePath(TEACHERS_RESOURCE + "/1/yoga-classes")
				.postWithContent(generateYogaClassJSONDTOWithDate(vinyasa(), 
						LocalDateTime.now().plusMinutes(15), 
						LocalDateTime.now().plusMinutes(45)),token);	
		assertEquals(HttpCode.OK.getCode(), createClass.getStatus());

		final Response reserveClass = resourceClient.resourcePath(RESERVATION_RESOURCE)
				.postWithContent(generateReservation(3,2), token);
		assertEquals(HttpCode.OK.getCode(), reserveClass.getStatus());
		
		final Response lateCancellation = resourceClient.resourcePath(TEACHERS_RESOURCE + "/1/yoga-classes/3/cancellations").get(token);
		assertEquals(HttpCode.FORBIDDEN.getCode(), lateCancellation.getStatus());
		assertJsonResponseWithFile(lateCancellation, "lateCancellation.json");
	}	
	
	@Test
	@RunAsClient
	public void testCancelYogaClassUnauthorizedTeacher() {		
		final Response responseGet = resourceClient.resourcePath(TEACHERS_RESOURCE + "/9999/yoga-classes/1/cancellations").get(token);
		assertEquals(HttpCode.FORBIDDEN.getCode(), responseGet.getStatus());
		assertJsonResponseWithFile(responseGet, "unauthorizedCancellation.json");
	}
	
	@Test
	@RunAsClient
	public void testCancelInexistentYogaClass() {		
		final Response responseGet = resourceClient.resourcePath(TEACHERS_RESOURCE + "/1/yoga-classes/9999/cancellations").get(token);
		assertEquals(HttpCode.NOT_FOUND.getCode(), responseGet.getStatus());
		assertJsonResponseWithFile(responseGet, "inexistentYogaClass.json");
	}
	
	@Test
	@RunAsClient
	public void testCancelExistingYogaClass() {		
		final Response responseGet = resourceClient.resourcePath(TEACHERS_RESOURCE + "/1/yoga-classes/1/cancellations").get(token);
		assertEquals(HttpCode.OK.getCode(), responseGet.getStatus());
		assertJsonResponseWithFile(responseGet, "authorizedCancellation.json");
	}
	
	@Test
	@RunAsClient
	public void testCompleteYogaClassUnauthorizedTeacher() {		
		final Response responseGet = resourceClient.resourcePath(TEACHERS_RESOURCE + "/9999/yoga-classes/2/completions").get(token);
		assertEquals(HttpCode.FORBIDDEN.getCode(), responseGet.getStatus());
		assertJsonResponseWithFile(responseGet, "unauthorizedCompletion.json");
	}
	
	@Test
	@RunAsClient
	public void testCompleteInexistentYogaClass() {		
		final Response responseGet = resourceClient.resourcePath(TEACHERS_RESOURCE + "/1/yoga-classes/9999/completions").get(token);
		assertEquals(HttpCode.NOT_FOUND.getCode(), responseGet.getStatus());
		assertJsonResponseWithFile(responseGet, "inexistentYogaClass.json");
	}
	
	@Test
	@RunAsClient
	public void testCompleteExistingYogaClass() {		
		final Response responseGet = resourceClient.resourcePath(TEACHERS_RESOURCE + "/1/yoga-classes/2/completions").get(token);
		assertEquals(HttpCode.OK.getCode(), responseGet.getStatus());
		assertJsonResponseWithFile(responseGet, "authorizedCompletion.json");
	}

	
	private void assertJsonResponseWithFile(final Response response, final String fileName) {
		assertJsonMatchesFileContent(response.readEntity(String.class), getPathFileResponse(TEACHERS_RESOURCE, fileName));
	}
}
