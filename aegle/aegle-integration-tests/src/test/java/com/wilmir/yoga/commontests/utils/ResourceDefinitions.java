package com.wilmir.yoga.commontests.utils;

public enum ResourceDefinitions {
	USERS("users"),
	YOGA_CLASSES("yoga-classes"),
	YOGA_RESERVATIONS("yoga-reservations"),
	YOGA_SEARCH("yoga-classes/search"),
	YOGA_CATEGORY("yoga-classes/category"),
	TEACHERS("teachers"),
	LOGIN("auth/login"),
	LOGOUT("auth/logout"), 
	STUDENT_CANCELLATIONS("yoga-cancellations"), 
	STUDENTS("students");
	
	private final String resourceName;

	private ResourceDefinitions(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getResourceName() {
		return resourceName;
	}
	
	
}
