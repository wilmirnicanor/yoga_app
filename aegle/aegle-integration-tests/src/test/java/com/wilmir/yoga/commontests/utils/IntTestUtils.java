package com.wilmir.yoga.commontests.utils;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import javax.ws.rs.core.Response;

import org.junit.Ignore;

import com.wilmir.yoga.common.model.HttpCode;


@Ignore
public class IntTestUtils {

	public static String findById(final ResourceClient resourceClient, final String pathResource, final Long elementId, final String token) {
		final Response response = resourceClient.resourcePath(pathResource + "/" + elementId).get(token);
		assertThat(response.getStatus(), is(equalTo(HttpCode.OK.getCode())));
		return response.readEntity(String.class);
	}

	private static Long assertResponseIsCreatedAndGetId(final Response response) {
		assertThat(response.getStatus(), is(equalTo(HttpCode.CREATED.getCode())));
		final Long elementId = JsonTestUtils.getIdFromJson(response.readEntity(String.class));
		assertThat(elementId, is(notNullValue()));
		return elementId;
	}

}