package com.wilmir.yoga.services.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.wilmir.yoga.dao.UserDAO;
import com.wilmir.yoga.services.AuthService;
import com.wilmir.yoga.services.PasswordCryptoService;

import static com.wilmir.yoga.commontests.user.UsersForTestsRepositoryUtil.*;


class AuthServiceImplUTest {
	private static final int TEACHER_ID = 1;
	private static final String PASSWORD = "123456";
	private UserDAO userDAO;
	private AuthService authService;
	private PasswordCryptoService passwordService = mock(PasswordCryptoService.class);
	
	@BeforeEach
	public void initTestCase() {
		userDAO = mock(UserDAO.class);
		authService = new AuthServiceImpl();
		((AuthServiceImpl) authService).userDAO = userDAO;
		((AuthServiceImpl) authService).passwordService = passwordService;
	}
	
	@Test
	public void testValidUserCorrectPassword() {
		when(userDAO.findByEmail(teacher().getEmail())).thenReturn(Optional.ofNullable(userWithId(teacher(), TEACHER_ID)));
		when(passwordService.encrypt(PASSWORD)).thenReturn(teacher().getPassword());
		assertTrue(authService.authenticate(teacher().getEmail(), PASSWORD));
		verify(userDAO, times(1)).findByEmail(teacher().getEmail());
	}
	
	@Test
	public void testValidUserIncorrectPassword() {
		when(userDAO.findByEmail(teacher().getEmail())).thenReturn(Optional.ofNullable(userWithId(teacher(), TEACHER_ID)));
		assertFalse(authService.authenticate(teacher().getEmail(), PASSWORD));
		verify(userDAO, times(1)).findByEmail(teacher().getEmail());
	}
	
	@Test
	public void testInexistentUser() {
		when(userDAO.findByEmail(teacher().getEmail())).thenReturn(Optional.empty());
		assertFalse(authService.authenticate(teacher().getEmail(), PASSWORD));
		verify(userDAO, times(1)).findByEmail(anyString());
	}
	
	@Test
	public void testNullEmail() {
		assertFalse(authService.authenticate(null, PASSWORD));
		verify(userDAO, never()).findByEmail(anyString());
	}
	
	@Test
	public void testNullPassword() {
		assertFalse(authService.authenticate(teacher().getEmail(), null));
		verify(userDAO, never()).findByEmail(anyString());
	}
	
	
	@Test
	public void testEmptyEmail() {
		assertFalse(authService.authenticate("", PASSWORD));
		verify(userDAO, never()).findByEmail(anyString());
	}
	
	@Test
	public void testEmptyPassword() {
		assertFalse(authService.authenticate(teacher().getEmail(), ""));
		verify(userDAO, never()).findByEmail(anyString());
	}
	
	
}
