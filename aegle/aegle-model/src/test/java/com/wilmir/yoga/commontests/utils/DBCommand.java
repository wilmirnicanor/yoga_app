package com.wilmir.yoga.commontests.utils;

import org.junit.Ignore;

@Ignore
public interface DBCommand<T> {

	T execute();

}