package com.wilmir.yoga.services.impl;

import static com.wilmir.yoga.commontests.yogaclass.YogaClassesForTestsRepositoryUtil.*;
import static org.junit.jupiter.api.Assertions.*;

import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.wilmir.yoga.common.exception.FieldNotValidException;
import com.wilmir.yoga.model.YogaClass;
import com.wilmir.yoga.services.FieldValidationService;

class FieldValidationServiceImplTest {
	private FieldValidationService validationService;
	private final YogaClass NEWYOGACLASS = yogaWithId(vinyasa(), 1);
	private Validator validator;

	@BeforeEach
	public void initTestCase() {
		validationService = new FieldValidationServiceImpl();
		validator = Validation.buildDefaultValidatorFactory().getValidator();
		((FieldValidationServiceImpl) validationService).validator = validator;
	}
	
	
	@Test
	public void testInValidYogaClassNullYogaType() {
		NEWYOGACLASS.setYogaType(null);
		final Throwable exception = assertThrows(FieldNotValidException.class, () -> validationService.validateFields(NEWYOGACLASS));
		assertEquals("yogaType", ((FieldNotValidException)exception).getInvalidFieldName());
	}
	
	@Test
	public void testInValidYogaClassEmptyYogaType() {
		NEWYOGACLASS.setYogaType("");
		final Throwable exception = assertThrows(FieldNotValidException.class, () -> validationService.validateFields(NEWYOGACLASS));
		assertEquals("yogaType", ((FieldNotValidException)exception).getInvalidFieldName());	
	}
	
	@Test
	public void testInValidYogaClassNullDescription() {
		NEWYOGACLASS.setDescription(null);
		final Throwable exception = assertThrows(FieldNotValidException.class, () -> validationService.validateFields(NEWYOGACLASS));
		assertEquals("description", ((FieldNotValidException)exception).getInvalidFieldName());
	}
	
	@Test
	public void testInValidYogaClassEmptyDescription() {
		NEWYOGACLASS.setDescription("");
		final Throwable exception = assertThrows(FieldNotValidException.class, () -> validationService.validateFields(NEWYOGACLASS));
		assertEquals("description", ((FieldNotValidException)exception).getInvalidFieldName());	
	}
	
	
	
	@Test
	public void testInValidYogaClassZeroCapacity() {
		NEWYOGACLASS.setCapacity(0);
		final Throwable exception = assertThrows(FieldNotValidException.class, () -> validationService.validateFields(NEWYOGACLASS));
		assertEquals("capacity", ((FieldNotValidException)exception).getInvalidFieldName());	
	}
	
	@Test
	public void testInValidYogaNullStatus() {
		NEWYOGACLASS.setStatus(null);
		final Throwable exception = assertThrows(FieldNotValidException.class, () -> validationService.validateFields(NEWYOGACLASS));
		assertEquals("status", ((FieldNotValidException)exception).getInvalidFieldName());	
	}
	
	@Test
	public void testInValidYogaEmptyStatus() {
		NEWYOGACLASS.setStatus("");
		final Throwable exception = assertThrows(FieldNotValidException.class, () -> validationService.validateFields(NEWYOGACLASS));
		assertEquals("status", ((FieldNotValidException)exception).getInvalidFieldName());	
	}
	
	@Test
	public void testInValidYogaNullLocation() {
		NEWYOGACLASS.setLocation(null);
		final Throwable exception = assertThrows(FieldNotValidException.class, () -> validationService.validateFields(NEWYOGACLASS));
		assertEquals("location", ((FieldNotValidException)exception).getInvalidFieldName());	
	}
	
	@Test
	public void testInValidYogaEmptyLocation() {
		NEWYOGACLASS.setLocation("");
		final Throwable exception = assertThrows(FieldNotValidException.class, () -> validationService.validateFields(NEWYOGACLASS));
		assertEquals("location", ((FieldNotValidException)exception).getInvalidFieldName());	
	}
	
	@Test
	public void testInValidYogaNullimageBase64Encoding() {
		NEWYOGACLASS.setImageBase64Encoding(null);
		final Throwable exception = assertThrows(FieldNotValidException.class, () -> validationService.validateFields(NEWYOGACLASS));
		assertEquals("imageBase64Encoding", ((FieldNotValidException)exception).getInvalidFieldName());	
	}
	
	@Test
	public void testInValidYogaimageBase64Encoding() {
		NEWYOGACLASS.setImageBase64Encoding("");
		final Throwable exception = assertThrows(FieldNotValidException.class, () -> validationService.validateFields(NEWYOGACLASS));
		assertEquals("imageBase64Encoding", ((FieldNotValidException)exception).getInvalidFieldName());	
	}
	
	@Test
	public void testInValidYogaNullStartTime() {
		NEWYOGACLASS.setStartTime(null);
		final Throwable exception = assertThrows(FieldNotValidException.class, () -> validationService.validateFields(NEWYOGACLASS));
		assertEquals("startTime", ((FieldNotValidException)exception).getInvalidFieldName());	
	}
	
	@Test
	public void testInvalidYogaNullCreatedTime() {
		NEWYOGACLASS.setCreatedTime(null);
		final Throwable exception = assertThrows(FieldNotValidException.class, () -> validationService.validateFields(NEWYOGACLASS));
		assertEquals("createdTime", ((FieldNotValidException)exception).getInvalidFieldName());	
	}
	
	@Test
	public void testInvalidYogaNullEndTime() {
		NEWYOGACLASS.setEndTime(null);
		final Throwable exception = assertThrows(FieldNotValidException.class, () -> validationService.validateFields(NEWYOGACLASS));
		assertEquals("endTime", ((FieldNotValidException)exception).getInvalidFieldName());	
	}

	@Test
	public void testAValidYogaClass() {
		validationService.validateFields(yogaWithId(vinyasa(), 1));
	}
	
}
