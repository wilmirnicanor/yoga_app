package com.wilmir.yoga.dao;


import static com.wilmir.yoga.commontests.user.UsersForTestsRepositoryUtil.*;
import static com.wilmir.yoga.commontests.yogaclass.YogaClassesForTestsRepositoryUtil.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.wilmir.yoga.commontests.utils.DBCommandTransactionalExecutor;
import com.wilmir.yoga.model.Student;
import com.wilmir.yoga.model.User;
import com.wilmir.yoga.model.YogaClass;


class YogaClassDAOTest {	
	private static final int INEXISTENT_STUDENT = 9999999;
	private static final int INEXISTENT_YOGA_CLASS = 9999999;
	private EntityManagerFactory emf;
	private EntityManager entityManager;
	private YogaClassDAO yogaClassDAO;
	private UserDAO userDAO;
	private DBCommandTransactionalExecutor dBCommandTransactionalExecutor;

	@BeforeEach
	public void initTestCase() {
		emf = Persistence.createEntityManagerFactory("inMemoryYogaPU");
		entityManager = emf.createEntityManager();
		userDAO = new UserDAO();
		yogaClassDAO = new YogaClassDAO();
		userDAO.entityManager = entityManager;
		yogaClassDAO.entityManager = entityManager;
		dBCommandTransactionalExecutor = new DBCommandTransactionalExecutor(entityManager);
	}

	@AfterEach
	public void closeEntityManager() {
		entityManager.close();
		emf.close();
	}

	@Test
	public void testAddYogaClassAndFindIt() {
		final int userId = dBCommandTransactionalExecutor.executeCommand(() -> userDAO.add(teacher()).getId());
		final int yogaId = dBCommandTransactionalExecutor.executeCommand(() -> yogaClassDAO.add(vinyasa(), userId).getId());
		assertThat(yogaId, is(notNullValue()));
		final Optional<YogaClass> yogaClassOptional = yogaClassDAO.findById(yogaId);
		assertTrue(yogaClassOptional.isPresent());
		yogaClassOptional.ifPresent((yogaClass) -> {			
			assertThat(yogaClass, is(notNullValue()));
			assertEquals(vinyasa().getYogaType(), yogaClass.getYogaType());
			assertEquals(vinyasa().getDescription(), yogaClass.getDescription());
			assertEquals(vinyasa().getCapacity(), yogaClass.getCapacity());
			assertEquals(vinyasa().getLocation(), yogaClass.getLocation());
			assertEquals(vinyasa().getStartTime().withSecond(0).withNano(0), yogaClass.getStartTime().withSecond(0).withNano(0));
			assertEquals(vinyasa().getEndTime().withSecond(0).withNano(0), yogaClass.getEndTime().withSecond(0).withNano(0));
			assertEquals(vinyasa().getStatus(), yogaClass.getStatus());
			assertEquals(vinyasa().getImageBase64Encoding(), yogaClass.getImageBase64Encoding());
			assertEquals(userWithId(teacher(),userId), yogaClass.getTeacher());
		});
	}
	
	@Test
	public void testUpdateAYogaClass() {
		final int userId = dBCommandTransactionalExecutor.executeCommand(() -> userDAO.add(teacher()).getId());
		final int yogaId = dBCommandTransactionalExecutor.executeCommand(() -> yogaClassDAO.add(vinyasa(), userId).getId());
		assertThat(yogaId, is(notNullValue()));
		final Optional<YogaClass> yogaClassOptional = yogaClassDAO.findById(yogaId);
		assertTrue(yogaClassOptional.isPresent());
		
		final YogaClass yogaClass = yogaClassOptional.get();
		yogaClass.setStatus("Cancelled");
		yogaClass.setCapacity(100);
		
		dBCommandTransactionalExecutor.executeCommand(() -> yogaClassDAO.updateYogaClass(yogaClass));

		final Optional<YogaClass> updatedYogaClassOptional = yogaClassDAO.findById(yogaId);
		assertTrue(updatedYogaClassOptional.isPresent());
		final YogaClass updatedYogaClass = yogaClassOptional.get();
		assertEquals("Cancelled", updatedYogaClass.getStatus());
		assertEquals(100, updatedYogaClass.getCapacity());
	}


	@Test
	public void testFindInexistentYogaClass() {		
		assertFalse(yogaClassDAO.findById(INEXISTENT_YOGA_CLASS).isPresent());
	}


	@Test
	public void testFindAllClasses() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();
			allClasses().forEach(yogaClass -> yogaClassDAO.add(yogaClass, teacherID));
			return teacherID;
		});
		final List<YogaClass> classes = yogaClassDAO.findAllClasses();
		assertEquals(allClasses().size(), classes.size());
		assertEquals(vinyasa().getYogaType(), classes.get(0).getYogaType());
		assertEquals(hatha().getYogaType(), classes.get(1).getYogaType());
		assertEquals(bikram().getYogaType(), classes.get(2).getYogaType());
	}

	
	@Test
	public void testFindAllClassesByTeacherTwoClasses() {
		final int userId = dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();
			yogaClassDAO.add(vinyasa(), teacherID);
			yogaClassDAO.add(bikram(), teacherID);
			return teacherID;
		});
		final List<YogaClass> classes = yogaClassDAO.findAllClassesByTeacherID(userId);
		assertEquals(2, classes.size());
		assertEquals(vinyasa().getYogaType(), classes.get(0).getYogaType());
		assertEquals(bikram().getYogaType(), classes.get(1).getYogaType());
	}


	@Test
	public void testFindAllClassesByTeacherZeroClass() {
		final int userId = dBCommandTransactionalExecutor.executeCommand(() -> userDAO.add(teacher()).getId());
		final List<YogaClass> classes = yogaClassDAO.findAllClassesByTeacherID(userId);
		assertEquals(0, classes.size());
	}


	@Test
	public void testFindAnExistingYogaClassesBetweenTwoDates() {
		final int userId = dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();
			yogaClassDAO.add(vinyasa(), teacherID);
			return teacherID;
		});
		final YogaClass newClass = vinyasa();
		newClass.setStartTime(vinyasa().getStartTime().minusHours(1));
		newClass.setEndTime(vinyasa().getEndTime().plusHours(1));
		assertTrue(yogaClassDAO.findOverlappingClasses(newClass, userId));

	}

	@Test
	public void testFindAnExistingYogaClassesWithOverlappingStartTime() {
		final int userId = dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();
			yogaClassDAO.add(vinyasa(), teacherID);
			return teacherID;
		});
		final YogaClass newClass = vinyasa();
		newClass.setStartTime(vinyasa().getStartTime().plusMinutes(30));
		newClass.setEndTime(vinyasa().getEndTime().plusMinutes(30));
		assertTrue(yogaClassDAO.findOverlappingClasses(newClass, userId));
	}

	@Test
	public void testFindAnExistingYogaClassesWithOverlappingEndTime() {
		final int userId = dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();
			yogaClassDAO.add(vinyasa(), teacherID);
			return teacherID;
		});
		final YogaClass newClass = vinyasa();
		newClass.setStartTime(vinyasa().getStartTime().minusMinutes(30));
		newClass.setEndTime(vinyasa().getEndTime().minusMinutes(30));
		assertTrue(yogaClassDAO.findOverlappingClasses(newClass, userId));
	}

	@Test
	public void testFindAnExistingYogaClassesWithSameStartAndEndTime() {
		final int userId = dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();
			yogaClassDAO.add(vinyasa(), teacherID);
			return teacherID;
		});
		assertTrue(yogaClassDAO.findOverlappingClasses(vinyasa(), userId));
	}


	@Test
	public void testFindAnExistingYogaClassesByDateInvalidTeacherID() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();
			yogaClassDAO.add(vinyasa(), teacherID);
			return teacherID;
		});
		final YogaClass newClass = vinyasa();
		newClass.setStartTime(vinyasa().getStartTime().plusMinutes(30));
		newClass.setEndTime(vinyasa().getEndTime().plusMinutes(30));
		assertFalse(yogaClassDAO.findOverlappingClasses(newClass, 999));
	}	

	
	@Test
	public void testSearchAClassByYogaTypeKeyMatch() {
		final YogaClass yogaClass = vinyasa();
		yogaClass.setStartTime(LocalDateTime.now().plusWeeks(10));
		dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();
			yogaClassDAO.add(yogaClass, teacherID);
			return null;
		});
		final List<YogaClass> searchResult = yogaClassDAO.searchClasses("vinyasa");
		assertEquals(1, searchResult.size());
	}
	
	
	@Test
	public void testSearchAClassByLocationKeyMatch() {
		final YogaClass yogaClass = vinyasa();
		yogaClass.setStartTime(LocalDateTime.now().plusWeeks(10));
		yogaClass.setLocation("Palawan, Philippines");
		dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();
			yogaClassDAO.add(yogaClass, teacherID);
			return null;
		});
		final List<YogaClass> searchResult = yogaClassDAO.searchClasses("pAlAwAn");
		assertEquals(1, searchResult.size());
	}
	
	
	@Test
	public void testSearchAClassByDescriptionKeyMatch() {
		final YogaClass yogaClass = vinyasa();
		yogaClass.setStartTime(LocalDateTime.now().plusWeeks(10));
		yogaClass.setDescription("Set your intentions");
		dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();
			yogaClassDAO.add(yogaClass, teacherID);
			return null;
		});
		final List<YogaClass> searchResult = yogaClassDAO.searchClasses("intent");
		assertEquals(1, searchResult.size());
	}

	
	@Test
	public void testSearchAClassZeroMatchDueToCompleteStatus() {
		final YogaClass yogaClass = vinyasa();
		yogaClass.setStartTime(LocalDateTime.now().plusWeeks(10));
		yogaClass.setStatus("Completed");
		dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();
			yogaClassDAO.add(yogaClass, teacherID);
			return null;
		});
		final List<YogaClass> searchResult = yogaClassDAO.searchClasses("vinyasa");
		assertEquals(0, searchResult.size());
	}
	
	@Test
	public void testSearchAClassZeroMatchDueToCanceledStatus() {
		final YogaClass yogaClass = vinyasa();
		yogaClass.setStartTime(LocalDateTime.now().plusWeeks(10));
		yogaClass.setStatus("Cancelled");
		dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();
			yogaClassDAO.add(yogaClass, teacherID);
			return null;
		});
		final List<YogaClass> searchResult = yogaClassDAO.searchClasses("vinyasa");
		assertEquals(0, searchResult.size());
	}
	
	@Test
	public void testGetCategory() {
		final YogaClass yogaClass = vinyasa();
		yogaClass.setStartTime(LocalDateTime.now().plusWeeks(10));
		yogaClass.setLocation("Palawan, Philippines");
		dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();
			yogaClassDAO.add(yogaClass, teacherID);
			return null;
		});
		final List<YogaClass> searchResult = yogaClassDAO.getCategory("vinyasa");
		assertEquals(1, searchResult.size());
	}

	@Test
	public void  testBookAClass() {
		// add new student
		final int studentID = dBCommandTransactionalExecutor.executeCommand(() -> {
			return userDAO.add(student()).getId();
		});
		// add new yoga class 
		final int yogaClassID = dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();	
			return yogaClassDAO.add(vinyasa(), teacherID).getId();
		});
		// verify student and yoga class are stored
		// verify student and yoga class are stored
		final Optional<User> studentOptional = userDAO.findById(studentID);
		assertTrue(studentOptional.isPresent());		
		final Optional<YogaClass> yogaClassOptional = yogaClassDAO.findById(yogaClassID);
		assertTrue(yogaClassOptional.isPresent());
		final Student student = (Student) studentOptional.get();
		final YogaClass yogaClass = yogaClassOptional.get();
		assertEquals(0, student.getClasses ().size());
		// add student to yogaClass
		final Student updatedStudent = dBCommandTransactionalExecutor.executeCommand(() -> {
			yogaClass.add(student);
			return userDAO.update(student);
		});
		// verify student is updated
		assertEquals(1, updatedStudent.getClasses().size());
		// verify class is updated
		final YogaClass updatedYogaClass = yogaClassDAO.findById(yogaClassID).get();
		assertEquals(1, updatedYogaClass.getStudents().size());
		assertTrue(updatedYogaClass.hasStudent(updatedStudent));
		// verify the correct student is added
		assertEquals(updatedStudent, updatedYogaClass.getStudents().iterator().next());

	}


	@Test
	public void  testFullyBookAClass() {
		// add new student
		final int studentID = dBCommandTransactionalExecutor.executeCommand(() -> {
			return userDAO.add(student()).getId();
		});
		// add new yoga class 
		final int yogaClassID = dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();	

			YogaClass yogaClass = vinyasa();
			yogaClass.setCapacity(1);

			return yogaClassDAO.add(yogaClass, teacherID).getId();
		});
		// verify student and yoga class are stored
		final Optional<User> studentOptional = userDAO.findById(studentID);
		final Optional<YogaClass> yogaClassOptional = yogaClassDAO.findById(yogaClassID);
		assertTrue(yogaClassOptional.isPresent());
		assertTrue(studentOptional.isPresent());
		assertTrue(yogaClassOptional.isPresent());
		final Student student = (Student) studentOptional.get();
		final YogaClass yogaClass = yogaClassOptional.get();
		dBCommandTransactionalExecutor.executeCommand(() -> {
			yogaClass.add(student);
			return userDAO.update(student);
		});
		// verify class is updated to Full
		final YogaClass updatedYogaClass = yogaClassDAO.findById(yogaClassID).get();
		assertEquals("Full", updatedYogaClass.getStatus());

	}



	@Test
	public void  testFindClassesByStudentIdExistingStudentZeroClass() {
		final int studentID = dBCommandTransactionalExecutor.executeCommand(() -> {
			return userDAO.add(student()).getId();
		});	
		final Optional<Set<YogaClass>> yogaClassesOptional = yogaClassDAO.findAllClassesByStudentID(studentID);
		assertTrue(yogaClassesOptional.isPresent());
		assertEquals(0, yogaClassesOptional.get().size());
	}


	@Test
	public void  testFindClassesByStudentIdExistingStudentOneClass() {
		final int studentID = dBCommandTransactionalExecutor.executeCommand(() -> {
			return userDAO.add(student()).getId();
		});
		final int yogaClassID = dBCommandTransactionalExecutor.executeCommand(() -> {
			final int teacherID = userDAO.add(teacher()).getId();	
			return yogaClassDAO.add(vinyasa(), teacherID).getId();
		});
		final Optional<User> studentOptional = userDAO.findById(studentID);
		final Optional<YogaClass> yogaClassOptional = yogaClassDAO.findById(yogaClassID);
		assertTrue(studentOptional.isPresent());
		assertTrue(yogaClassOptional.isPresent());
		final Student student = (Student) studentOptional.get();
		final YogaClass yogaClass = yogaClassOptional.get();
		dBCommandTransactionalExecutor.executeCommand(() -> {
			yogaClass.add(student);
			return userDAO.update(student);
		});
		final Optional<Set<YogaClass>> yogaClassesOptional = yogaClassDAO.findAllClassesByStudentID(studentID);
		assertTrue(yogaClassesOptional.isPresent());
		assertEquals(1, yogaClassesOptional.get().size());
	}


	@Test
	public void  testFindClassesByStudentIdEInexistentStudent() {		
		assertFalse(yogaClassDAO.findAllClassesByStudentID(INEXISTENT_STUDENT).isPresent());
	}
}
