package com.wilmir.yoga.services.impl;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.wilmir.yoga.services.PasswordCryptoService;

class PasswordServiceImplTest {
	
	private PasswordCryptoService passwordService = new PasswordCryptoServiceImpl();

	@Test
	void testPasswordEncryption() {
		assertEquals("jZae727K08KaOmKSgOaGzww/XVqGr/PKEgIMkjrcbJI=", passwordService.encrypt("123456"));
	}
	

}
