package com.wilmir.yoga.commontests.yogaclass;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.junit.Ignore;

import com.wilmir.yoga.model.Teacher;
import com.wilmir.yoga.model.YogaClass;

@Ignore
public class YogaClassesForTestsRepositoryUtil {

	private final static String BASE64IMAGE = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAACXBIWXMA";
	
	public static YogaClass vinyasa() {
		return new YogaClass("Vinyasa", "Set your intentions before you start the day", 15, "Dublin, Ireland", 
				LocalDateTime.now().plusHours(1), LocalDateTime.now().plusHours(2), BASE64IMAGE);
	}

	public static YogaClass hatha() {
		return new YogaClass("Hatha", "Improve your focus and endurance with Hatha", 5, "Athlone, Ireland", 
				LocalDateTime.now().plusHours(2), LocalDateTime.now().plusHours(3), BASE64IMAGE);
	}
	
	public static YogaClass bikram() {
		return new YogaClass("Bikram", "Fire up your morning with Bikram", 10, "Cork, Ireland", 
				LocalDateTime.now().plusHours(3), LocalDateTime.now().plusHours(4), BASE64IMAGE);
	}
	
	public static YogaClass yogaWithId(final YogaClass yogaClass, final int userId) {
		yogaClass.setId(userId);
		return yogaClass;
	}
	
	public static YogaClass yogaWithTeacher(final YogaClass yogaClass, final Teacher teacher) {
		yogaClass.setTeacher(teacher);
		return yogaClass;
	}
	
	public static List<YogaClass> allClasses() {
		return Arrays.asList(vinyasa(), hatha(), bikram());
	}
	
	public static List<YogaClass> listWithARegisteredVinyasaClass() {
		return Arrays.asList(yogaWithId(vinyasa(),1));
	}
	
	public static List<YogaClass> listWithMultipleYogaClasses(){
		return Arrays.asList(yogaWithId(hatha(),3), yogaWithId(vinyasa(),2), yogaWithId(bikram(),1));
	}
	
	public static List<YogaClass> listWithMultipleVinyasaClasses(){		
		return Arrays.asList(yogaWithId(vinyasa(),3), yogaWithId(vinyasa(),2), yogaWithId(vinyasa(),1));
	}

}