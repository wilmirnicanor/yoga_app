package com.wilmir.yoga.commontests.user;

import java.util.Arrays;
import java.util.List;

import org.junit.Ignore;

import com.wilmir.yoga.model.Student;
import com.wilmir.yoga.model.Teacher;
import com.wilmir.yoga.model.User;

@Ignore
public class UsersForTestsRepositoryUtil {

	public static User teacher() {
		return new Teacher("Almira Romer", "teacher@aegle.com", "jZae727K08KaOmKSgOaGzww/XVqGr/PKEgIMkjrcbJI=", "picture.jpg");
	}

	public static User student() {
		return new Student("Wilmir Nicanor", "student@aegle.com", "jZae727K08KaOmKSgOaGzww/XVqGr/PKEgIMkjrcbJI=", "picture.jpg");
	}
	
	public static User userWithId(final User user, final int userId) {
		user.setId(userId);
		return user;
	}
	
	public static List<User> allUsers() {
		return Arrays.asList(teacher(), student());
	}

}