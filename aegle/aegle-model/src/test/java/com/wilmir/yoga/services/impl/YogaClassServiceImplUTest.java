package com.wilmir.yoga.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.wilmir.yoga.common.exception.LateCancellationException;
import com.wilmir.yoga.common.exception.OverlappingYogaDateTimeException;
import com.wilmir.yoga.common.exception.UnauthorizedCancellationException;
import com.wilmir.yoga.common.exception.UserNotFoundException;
import com.wilmir.yoga.common.exception.YogaClassAlreadyBookedException;
import com.wilmir.yoga.common.exception.YogaClassFullException;
import com.wilmir.yoga.common.exception.YogaClassNotFoundException;
import com.wilmir.yoga.dao.UserDAO;
import com.wilmir.yoga.dao.YogaClassDAO;
import com.wilmir.yoga.model.Student;
import com.wilmir.yoga.model.Teacher;
import com.wilmir.yoga.model.YogaClass;
import com.wilmir.yoga.services.FieldValidationService;
import com.wilmir.yoga.services.YogaClassService;
import static com.wilmir.yoga.commontests.yogaclass.YogaClassesForTestsRepositoryUtil.*;
import static com.wilmir.yoga.commontests.user.UsersForTestsRepositoryUtil.*;


public class YogaClassServiceImplUTest {
	private static final int UNAUTHORIZED_TEACHER_ID = 9999;
	private static final int TEACHER_ID = 1;
	private static final int STUDENT_ID = 2;
	private static final int STUDENT_ID2 = 3;
	private static final int YOGA_ID = 1;
	private YogaClassService yogaClassService;
	private FieldValidationService validationService;
	private Validator validator;
	private YogaClassDAO yogaClassDAO;
	private UserDAO userDAO;

	@BeforeEach
	public void initTestCase() {
		validationService = new FieldValidationServiceImpl();
		validator = Validation.buildDefaultValidatorFactory().getValidator();
		((FieldValidationServiceImpl) validationService).validator = validator;
		yogaClassDAO = mock(YogaClassDAO.class);
		userDAO = mock(UserDAO.class);
		yogaClassService = new YogaClassServiceImpl();
		((YogaClassServiceImpl) yogaClassService).validationService = validationService;
		((YogaClassServiceImpl) yogaClassService).yogaClassDAO = yogaClassDAO;
		((YogaClassServiceImpl) yogaClassService).userDAO = userDAO;
	}
	
	@Test
	public void testAddYogaClassWithOverlappingDate() {		
		final Teacher teacher = (Teacher) userWithId(teacher(), TEACHER_ID);
		when(userDAO.findById(TEACHER_ID)).thenReturn(Optional.ofNullable(teacher));
		when(yogaClassDAO.findOverlappingClasses(anyObject(), anyInt())).thenReturn(true);
		final Throwable exception = assertThrows(OverlappingYogaDateTimeException.class, () -> yogaClassService.createClass(vinyasa(), 1));
		assertEquals("An existing class overlaps with the new class.", exception.getMessage());
		verify(userDAO, times(1)).findById(TEACHER_ID);
		verify(yogaClassDAO, times(1)).findOverlappingClasses(vinyasa(), TEACHER_ID);
		verify(yogaClassDAO, never()).add(anyObject(), anyInt());
	}
	
	@Test
	public void testAddYogaClassWithInexistentTeacher() {		
		when(userDAO.findById(TEACHER_ID)).thenReturn(Optional.empty());
		final Throwable exception = assertThrows(UserNotFoundException.class, () -> yogaClassService.createClass(vinyasa(), 1));
		assertEquals("The teacher is not found", exception.getMessage());
		verify(userDAO, times(1)).findById(TEACHER_ID);
		verify(yogaClassDAO, never()).findOverlappingClasses(vinyasa(), TEACHER_ID);
		verify(yogaClassDAO, never()).add(anyObject(), anyInt());
	}
	
	@Test
	public void testAddAValidYogaClass() {	
		final Teacher teacher = (Teacher) userWithId(teacher(), TEACHER_ID);
		when(userDAO.findById(TEACHER_ID)).thenReturn(Optional.ofNullable(teacher));
		yogaClassService.createClass(vinyasa(), TEACHER_ID);
		verify(userDAO, times(1)).findById(TEACHER_ID);
		verify(yogaClassDAO, times(1)).findOverlappingClasses(vinyasa(), TEACHER_ID);
		verify(yogaClassDAO, times(1)).add(vinyasa(), TEACHER_ID);
	}
	

	@Test
	public void testFindYogaClassByIdExisting() {
		when(yogaClassDAO.findById(YOGA_ID)).thenReturn(Optional.ofNullable(yogaWithId(vinyasa(),YOGA_ID)));
		assertEquals(yogaWithId(vinyasa(),YOGA_ID), yogaClassService.findById(YOGA_ID));
		verify(yogaClassDAO, times(1)).findById(YOGA_ID);
	}
	
	
	@Test
	public void testFindYogaClassByIdNotExisting() {
		when(yogaClassDAO.findById(YOGA_ID)).thenReturn(Optional.empty());
		final Throwable exception = assertThrows(YogaClassNotFoundException.class, () -> yogaClassService.findById(YOGA_ID));
		assertEquals("The requested yoga class is not found", exception.getMessage());
		verify(yogaClassDAO, times(1)).findById(YOGA_ID);
	}
	
	@Test
	public void testFindAllClasses() {
		when(yogaClassDAO.findAllClasses()).thenReturn(listWithMultipleYogaClasses());
		Collection<YogaClass> results = yogaClassService.findAllClasses();
		assertEquals(listWithMultipleYogaClasses().size(), results.size());
		assertEquals(vinyasa().getYogaType(), ((List<YogaClass>) results).get(0).getYogaType());
		assertEquals(hatha().getYogaType(), ((List<YogaClass>) results).get(1).getYogaType());
		assertEquals(bikram().getYogaType(), ((List<YogaClass>) results).get(2).getYogaType());
		verify(yogaClassDAO, times(1)).findAllClasses();
	}
	
	@Test
	public void testFindAllClassesByExistingTeacherID() {
		final Teacher teacher = (Teacher) userWithId(teacher(), TEACHER_ID);
		when(userDAO.findById(TEACHER_ID)).thenReturn(Optional.ofNullable(teacher));
		when(yogaClassDAO.findAllClassesByTeacherID(TEACHER_ID)).thenReturn(listWithMultipleYogaClasses());
		Collection<YogaClass> results = yogaClassService.findAllClassesByTeacherID(TEACHER_ID);
		assertEquals(listWithMultipleYogaClasses().size(), results.size());
		assertEquals(vinyasa().getYogaType(), ((List<YogaClass>) results).get(0).getYogaType());
		assertEquals(hatha().getYogaType(), ((List<YogaClass>) results).get(1).getYogaType());
		assertEquals(bikram().getYogaType(), ((List<YogaClass>) results).get(2).getYogaType());
		verify(userDAO, times(1)).findById(TEACHER_ID);
		verify(yogaClassDAO, times(1)).findAllClassesByTeacherID(TEACHER_ID);
	}
	
	@Test
	public void testFindAllClassesByInexistentTeacherID() {
		when(userDAO.findById(TEACHER_ID)).thenReturn(Optional.empty());
		when(yogaClassDAO.findAllClassesByTeacherID(TEACHER_ID)).thenReturn(listWithMultipleYogaClasses());
		final Throwable exception = assertThrows(UserNotFoundException.class, () -> yogaClassService.findAllClassesByTeacherID(TEACHER_ID));
		assertEquals("The teacher is not found", exception.getMessage());
		verify(userDAO, times(1)).findById(TEACHER_ID);
		verify(yogaClassDAO, never()).findAllClassesByTeacherID(TEACHER_ID);
	}
	
	
	@Test
	public void testSearchClasses() {
		when(yogaClassDAO.searchClasses("ireland")).thenReturn(listWithMultipleYogaClasses());
		Collection<YogaClass> results = yogaClassService.searchYogaClasses("ireland");
		assertEquals(listWithMultipleYogaClasses().size(), results.size());
		assertEquals(vinyasa().getYogaType(), ((List<YogaClass>) results).get(0).getYogaType());
		assertEquals(hatha().getYogaType(), ((List<YogaClass>) results).get(1).getYogaType());
		assertEquals(bikram().getYogaType(), ((List<YogaClass>) results).get(2).getYogaType());
		verify(yogaClassDAO, times(1)).searchClasses("ireland");
	}
	
	@Test
	public void testGetCategory() {
		when(yogaClassDAO.getCategory("vinyasa")).thenReturn(listWithMultipleVinyasaClasses());
		Collection<YogaClass> results = yogaClassService.getCategory("vinyasa");
		assertEquals(listWithMultipleVinyasaClasses().size(), results.size());
		assertEquals(vinyasa().getYogaType(), ((List<YogaClass>) results).get(0).getYogaType());
		assertEquals(vinyasa().getYogaType(), ((List<YogaClass>) results).get(1).getYogaType());
		assertEquals(vinyasa().getYogaType(), ((List<YogaClass>) results).get(2).getYogaType());
		verify(yogaClassDAO, times(1)).getCategory("vinyasa");
	}
	
	@Test
	public void testBookAYogaClassInexistentClass() {
		final Student student = (Student) userWithId(student(), STUDENT_ID);
		when(yogaClassDAO.findById(YOGA_ID)).thenReturn(Optional.empty());
		when(userDAO.findById(STUDENT_ID)).thenReturn(Optional.ofNullable(student));
		final Throwable exception = assertThrows(YogaClassNotFoundException.class, () -> yogaClassService.bookAYogaClass(YOGA_ID, STUDENT_ID));
		assertEquals("The requested yoga class is not found", exception.getMessage());
		verify(yogaClassDAO, times(1)).findById(YOGA_ID);
		verify(userDAO, times(1)).findById(STUDENT_ID);
		verify(userDAO, never()).update(anyObject());
	}
	
	@Test
	public void testBookAYogaClassFullClass() {
		final Student student1 = (Student) userWithId(student(), STUDENT_ID);
		final Student student2 = (Student) userWithId(student(), STUDENT_ID2);
		student2.setEmail("newEmail@aegle.com");
		final YogaClass yogaClass = yogaWithId(vinyasa(),YOGA_ID);
		yogaClass.setCapacity(1);
		yogaClass.add(student1);
		when(userDAO.findById(STUDENT_ID2)).thenReturn(Optional.ofNullable(student2));
		when(yogaClassDAO.findById(YOGA_ID)).thenReturn(Optional.ofNullable(yogaClass));		
		final Throwable exception = assertThrows(YogaClassFullException.class, () -> yogaClassService.bookAYogaClass(YOGA_ID, STUDENT_ID2));
		assertEquals("The requested yoga class is full", exception.getMessage());
		verify(yogaClassDAO, times(1)).findById(YOGA_ID);
		verify(userDAO, times(1)).findById(STUDENT_ID2);
		verify(userDAO, never()).update(anyObject());
	}
	
	@Test
	public void testBookAYogaClassInexistentStudent() {		
		when(yogaClassDAO.findById(YOGA_ID)).thenReturn(Optional.ofNullable(yogaWithId(vinyasa(),YOGA_ID)));
		when(userDAO.findById(STUDENT_ID)).thenReturn(Optional.empty());
		final Throwable exception = assertThrows(UserNotFoundException.class, () -> yogaClassService.bookAYogaClass(YOGA_ID, STUDENT_ID));
		assertEquals("The student is not found", exception.getMessage());
		verify(userDAO, times(1)).findById(STUDENT_ID);
		verify(yogaClassDAO, never()).findById(YOGA_ID);
		verify(userDAO, never()).update(anyObject());
	}
	
	
	@Test
	public void testBookAYogaClassOneValidRequest() {	
		final YogaClass yogaClass = yogaWithId(vinyasa(),YOGA_ID);
		final Student student = (Student) userWithId(student(), STUDENT_ID);
		when(yogaClassDAO.findById(YOGA_ID)).thenReturn(Optional.ofNullable(yogaClass));
		when(userDAO.findById(STUDENT_ID)).thenReturn(Optional.ofNullable(student));
		final YogaClass updatedYogaClass = yogaClassService.bookAYogaClass(YOGA_ID, STUDENT_ID);
		assertTrue(updatedYogaClass.hasStudent(student));
		assertEquals(1, updatedYogaClass.getStudents().size());
		assertEquals(1, student.getClasses().size());
		verify(yogaClassDAO, times(1)).findById(YOGA_ID);
		verify(userDAO, times(1)).findById(STUDENT_ID);
		verify(userDAO, times(1)).update(student);
	}
	
	@Test
	public void testBookAYogaClassTwoValidRequests() {	
		final YogaClass yogaClass = yogaWithId(vinyasa(),YOGA_ID);
		final Student student1 = (Student) userWithId(student(), STUDENT_ID);
		final Student student2 = (Student) userWithId(new Student("Jhon", "jhon@aegle.com", "123456", "picture.jpg"), STUDENT_ID2);	
		when(yogaClassDAO.findById(YOGA_ID)).thenReturn(Optional.ofNullable(yogaClass));
		when(userDAO.findById(STUDENT_ID)).thenReturn(Optional.ofNullable(student1));
		when(userDAO.findById(STUDENT_ID2)).thenReturn(Optional.ofNullable(student2));

		yogaClassService.bookAYogaClass(YOGA_ID, STUDENT_ID);
		yogaClassService.bookAYogaClass(YOGA_ID, STUDENT_ID2);

		verify(yogaClassDAO, times(2)).findById(YOGA_ID);
		verify(userDAO, times(1)).findById(STUDENT_ID);
		verify(userDAO, times(1)).findById(STUDENT_ID2);
		verify(userDAO, times(1)).update(student1);
		verify(userDAO, times(1)).update(student2);
	}
	
	@Test
	public void testDoubleBookingAYogaClass() {	
		final YogaClass yogaClass = yogaWithId(vinyasa(),YOGA_ID);
		final Student student = (Student) userWithId(student(), STUDENT_ID);
		when(yogaClassDAO.findById(YOGA_ID)).thenReturn(Optional.ofNullable(yogaClass));
		when(userDAO.findById(STUDENT_ID)).thenReturn(Optional.ofNullable(student));
		yogaClassService.bookAYogaClass(YOGA_ID, STUDENT_ID);

		final Throwable exception = assertThrows(YogaClassAlreadyBookedException.class, () -> yogaClassService.bookAYogaClass(YOGA_ID, STUDENT_ID));
		assertEquals("The student has already booked the class.", exception.getMessage());
		
		verify(yogaClassDAO, times(2)).findById(YOGA_ID);
		verify(userDAO, times(2)).findById(STUDENT_ID);
		verify(userDAO, times(1)).update(student);
	}
	
	@Test
	public void testCancelANonEmptyClassWhenItIsReady() {
		final YogaClass yogaClass = yogaWithId(vinyasa(),YOGA_ID);
		yogaClass.setStartTime(LocalDateTime.now().plusMinutes(20));
		yogaClass.setEndTime(LocalDateTime.now().plusMinutes(80));
		final Student student = (Student) userWithId(student(), STUDENT_ID);
		yogaClass.add(student);
		when(yogaClassDAO.findById(YOGA_ID)).thenReturn(Optional.ofNullable(yogaClass));
		
		final Throwable exception = assertThrows(LateCancellationException.class, () -> yogaClassService.cancelYogaClassByTeacher(YOGA_ID, TEACHER_ID));
		assertEquals("Cancellation cannot be made an hour before a class with students", exception.getMessage());
		verify(yogaClassDAO, times(1)).findById(YOGA_ID);
		verify(yogaClassDAO, never()).updateYogaClass(anyObject());
	}
	
	@Test
	public void testUnauthorizedCancellation() {
		final YogaClass yogaClass = yogaWithId(vinyasa(),YOGA_ID);
		yogaClass.setStartTime(LocalDateTime.now().plusMinutes(20));
		yogaClass.setEndTime(LocalDateTime.now().plusMinutes(80));
		final Teacher teacher = (Teacher) userWithId(teacher(), TEACHER_ID);
		yogaClass.setTeacher(teacher);
		when(yogaClassDAO.findById(YOGA_ID)).thenReturn(Optional.ofNullable(yogaClass));	
		when(yogaClassDAO.updateYogaClass(anyObject())).thenReturn(yogaClass);	
		final Throwable exception = assertThrows(UnauthorizedCancellationException.class, () -> yogaClassService.cancelYogaClassByTeacher(YOGA_ID, UNAUTHORIZED_TEACHER_ID));
		assertEquals("The cancellation of this class is forbidden.", exception.getMessage());
		verify(yogaClassDAO, times(1)).findById(YOGA_ID);
		verify(yogaClassDAO, never()).updateYogaClass(anyObject());
	}
	
	@Test
	public void testCancelAnEmptyClassWhenItIsReady() {
		final YogaClass yogaClass = yogaWithId(vinyasa(),YOGA_ID);
		yogaClass.setStartTime(LocalDateTime.now().plusMinutes(20));
		yogaClass.setEndTime(LocalDateTime.now().plusMinutes(80));
		final Teacher teacher = (Teacher) userWithId(teacher(), TEACHER_ID);
		yogaClass.setTeacher(teacher);
		when(yogaClassDAO.findById(YOGA_ID)).thenReturn(Optional.ofNullable(yogaClass));	
		when(yogaClassDAO.updateYogaClass(anyObject())).thenReturn(yogaClass);		
		yogaClassService.cancelYogaClassByTeacher(YOGA_ID, TEACHER_ID);
		verify(yogaClassDAO, times(1)).findById(YOGA_ID);
		verify(yogaClassDAO, times(1)).updateYogaClass(isA(YogaClass.class));
	}
	
	@Test
	public void testCancelAnEmptyClassWhenItIsNotReady() {
		final YogaClass yogaClass = yogaWithId(vinyasa(),YOGA_ID);
		yogaClass.setStartTime(LocalDateTime.now().plusMinutes(80));
		yogaClass.setEndTime(LocalDateTime.now().plusMinutes(120));
		final Teacher teacher = (Teacher) userWithId(teacher(), TEACHER_ID);
		yogaClass.setTeacher(teacher);
		when(yogaClassDAO.findById(YOGA_ID)).thenReturn(Optional.ofNullable(yogaClass));	
		when(yogaClassDAO.updateYogaClass(anyObject())).thenReturn(yogaClass);		
		yogaClassService.cancelYogaClassByTeacher(YOGA_ID, TEACHER_ID);
		verify(yogaClassDAO, times(1)).findById(YOGA_ID);
		verify(yogaClassDAO, times(1)).updateYogaClass(isA(YogaClass.class));
	}
	
	@Test
	public void testCancelANonEmptyClassWhenItIsNotReady() {
		final YogaClass yogaClass = yogaWithId(vinyasa(),YOGA_ID);
		yogaClass.setStartTime(LocalDateTime.now().plusMinutes(80));
		yogaClass.setEndTime(LocalDateTime.now().plusMinutes(120));
		final Teacher teacher = (Teacher) userWithId(teacher(), TEACHER_ID);
		yogaClass.setTeacher(teacher);
		final Student student = (Student) userWithId(student(), STUDENT_ID);
		yogaClass.add(student);
		when(yogaClassDAO.findById(YOGA_ID)).thenReturn(Optional.ofNullable(yogaClass));
		when(yogaClassDAO.updateYogaClass(anyObject())).thenReturn(yogaClass);		
		yogaClassService.cancelYogaClassByTeacher(YOGA_ID, TEACHER_ID);
		verify(yogaClassDAO, times(1)).findById(YOGA_ID);
		verify(yogaClassDAO, times(1)).updateYogaClass(isA(YogaClass.class));
	}
}
