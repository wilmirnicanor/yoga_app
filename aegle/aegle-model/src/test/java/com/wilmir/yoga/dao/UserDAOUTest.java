package com.wilmir.yoga.dao;


import static com.wilmir.yoga.commontests.user.UsersForTestsRepositoryUtil.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.wilmir.yoga.commontests.utils.DBCommandTransactionalExecutor;
import com.wilmir.yoga.model.Student;
import com.wilmir.yoga.model.Teacher;
import com.wilmir.yoga.model.User;


class UserDAOUTest {	
	private EntityManagerFactory emf;
	private EntityManager entityManager;
	private UserDAO userDAO;
	private DBCommandTransactionalExecutor dBCommandTransactionalExecutor;

	@BeforeEach
	public void initTestCase() {
		emf = Persistence.createEntityManagerFactory("inMemoryYogaPU");
		entityManager = emf.createEntityManager();
		userDAO = new UserDAO();
		userDAO.entityManager = entityManager;
		dBCommandTransactionalExecutor = new DBCommandTransactionalExecutor(entityManager);
	}

	@AfterEach
	public void closeEntityManager() {
		entityManager.close();
		emf.close();
	}

	@Test
	public void addUserAndFindIt() {
		final int userId = dBCommandTransactionalExecutor.executeCommand(() -> {
			return userDAO.add(teacher()).getId();
		});
		assertThat(userId, is(notNullValue()));
		final Optional<User> user = userDAO.findById(userId);
		assertTrue(user.isPresent());
		assertEquals(teacher().getName(), user.get().getName());
		assertEquals(teacher().getEmail(), user.get().getEmail());
		assertEquals(teacher().getPassword(), user.get().getPassword());
		assertEquals(teacher().getUserType(), user.get().getUserType());
		assertEquals(teacher().getImage(), user.get().getImage());

	}
	
	@Test
	public void findUserByIdNotFound() {
		final Optional<User> user = userDAO.findById(999);
		assertFalse(user.isPresent());
	}
	
	@Test
	public void findUserByEmailExisting() {
		final int userId = dBCommandTransactionalExecutor.executeCommand(() -> {
			return userDAO.add(teacher()).getId();
		});
		final Optional<User> user = userDAO.findByEmail("teacher@aegle.com");
		assertTrue(user.isPresent());
		assertEquals(teacher().getName(), user.get().getName());
		assertEquals(userId, user.get().getId());
	}
	
	
	@Test
	public void findUserByEmailNotExisting() {
		final Optional<User> user = userDAO.findByEmail("teacher@aegle.com");
		assertFalse(user.isPresent());
	}
	
	
	@Test
	public void findAllTeachers() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			allUsers().forEach(userDAO::add);
			return null;
		});
		final List<Teacher> teachers = userDAO.findAllTeachers();
		assertEquals(1, teachers.size());
		assertEquals(teacher().getName(), teachers.get(0).getName());
	}
	

	@Test
	public void findAllStudents() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			allUsers().forEach(userDAO::add);
			return null;
		});
		final List<Student> students = userDAO.findAllStudents();
		assertEquals(1, students.size());
		assertEquals(student().getName(), students.get(0).getName());
	}
	
	
	@Test
	public void existsByEmail() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			allUsers().forEach(userDAO::add);
			return null;
		});
		assertTrue(userDAO.existsByEmail(student().getEmail()));
		assertTrue(userDAO.existsByEmail(teacher().getEmail()));
	}
	
	
	@Test
	public void notExistsByEmail() {
		assertFalse(userDAO.existsByEmail(student().getEmail()));
		assertFalse(userDAO.existsByEmail(teacher().getEmail()));
	}
	
	
}
