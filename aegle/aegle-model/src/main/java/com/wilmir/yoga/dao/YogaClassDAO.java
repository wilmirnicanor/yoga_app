package com.wilmir.yoga.dao;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.wilmir.yoga.model.Student;
import com.wilmir.yoga.model.Teacher;
import com.wilmir.yoga.model.YogaClass;

@Stateless
public class YogaClassDAO {
	private final static String YOGA_CLASS_QUERY = "SELECT DISTINCT u FROM YogaClass u LEFT JOIN FETCH u.students WHERE u.status NOT IN (:excludedStatuses)";
	private final static String FIND_YOGA_CLASS_BY_ID_QUERY = "SELECT u FROM YogaClass u LEFT JOIN FETCH u.students WHERE u.id = :id";
	private final static  String YOGA_CLASSES_BY_TEACHER_ID_QUERY = "SELECT DISTINCT u FROM YogaClass u LEFT JOIN FETCH u.students WHERE u.teacher.id = :teacherID";
	private final static  String YOGA_CLASSES_BY_STUDENT_ID_QUERY = "SELECT DISTINCT s FROM Student s LEFT JOIN FETCH s.classes WHERE s.id = :studentID";
	private final static  String YOGA_CLASSES_GET_CATEGORY = "SELECT DISTINCT u FROM YogaClass u LEFT JOIN FETCH u.students WHERE u.startTime >= :nowTime "
			+ "AND ((LOWER(u.yogaType) = :category)) "
			+ "AND u.status NOT IN (:excludedStatuses)";
	private final static String YOGA_CLASSES_SEARCH_QUERY = "SELECT DISTINCT u FROM YogaClass u LEFT JOIN FETCH u.students WHERE u.startTime >= :nowTime "
			+ "AND ((LOWER(u.yogaType) LIKE :searchKey) OR (LOWER(u.description) LIKE :searchKey) OR (LOWER(u.location) LIKE :searchKey)) "
			+ "AND (u.status NOT IN (:excludedStatuses))";

	
	@PersistenceContext
	EntityManager entityManager;
	
	public YogaClass add(final YogaClass yogaClass, final int teacherID) {
		final Teacher teacher = entityManager.getReference(Teacher.class, teacherID);
		yogaClass.setTeacher(teacher);
		entityManager.persist(yogaClass);
		return yogaClass;
	}
	
	public Optional<YogaClass> findById(final int yogaClassId) {
		final Query query = entityManager.createQuery(FIND_YOGA_CLASS_BY_ID_QUERY, YogaClass.class);
		query.setParameter("id", yogaClassId);
		YogaClass yogaClass;
		try {
			yogaClass = (YogaClass) query.getSingleResult();
		}catch(final NoResultException exception) {
			yogaClass = null;
		}
		return Optional.ofNullable(yogaClass);
	}
	
	public boolean findOverlappingClasses(final YogaClass yogaClass, final int teacherID) {
		return findAllClassesByTeacherID(teacherID).stream()
				.filter(currentClass -> {
			return checkOverlappingStartTime(yogaClass, currentClass) || checkOverlappingEndTime(yogaClass, currentClass) || checkCoveringTime(yogaClass, currentClass);
		}).filter(currentClass -> {
			return !("Cancelled").equalsIgnoreCase(currentClass.getStatus());
		}).count() > 0;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<YogaClass> getCategory(final String category){
		final String key = category.toLowerCase();
		final Query query = entityManager.createQuery(YOGA_CLASSES_GET_CATEGORY, YogaClass.class);
		query.setParameter("nowTime", LocalDateTime.now());
		query.setParameter("category", key);
		excludeCompleteCanceledAndExpiredClassesFrom(query);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<YogaClass> searchClasses(final String searchKey){
		final String key = "%" + searchKey.toLowerCase() + "%";
		final Query query = entityManager.createQuery(YOGA_CLASSES_SEARCH_QUERY, YogaClass.class);
		query.setParameter("nowTime", LocalDateTime.now());
		query.setParameter("searchKey", key);
		excludeCompleteCanceledAndExpiredClassesFrom(query);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<YogaClass> findAllClasses(){
		final Query query = entityManager.createQuery(YOGA_CLASS_QUERY, YogaClass.class);
		excludeCompleteCanceledAndExpiredClassesFrom(query);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<YogaClass> findAllClassesByTeacherID(final int teacherID){
		final Query query = entityManager.createQuery(YOGA_CLASSES_BY_TEACHER_ID_QUERY, YogaClass.class);
		query.setParameter("teacherID", teacherID);
		return query.getResultList();
	}
	
	public Optional<Set<YogaClass>> findAllClassesByStudentID(final int studentID){
		final Query query = entityManager.createQuery(YOGA_CLASSES_BY_STUDENT_ID_QUERY, Student.class);
		query.setParameter("studentID", studentID);
		Set<YogaClass> yogaClasses;
		try {
			final Student student = (Student) query.getSingleResult();
			yogaClasses = student.getClasses();
		}catch(final NoResultException exception) {
			yogaClasses = null;
		}	
		return Optional.ofNullable(yogaClasses);
	}
	
	public YogaClass updateYogaClass(final YogaClass yogaClass) {
		return entityManager.merge(yogaClass);
	}
	
	private boolean checkCoveringTime(final YogaClass yogaClass, final YogaClass currentClass) {
		return (currentClass.getStartTime().isAfter(yogaClass.getStartTime()) 
				|| currentClass.getStartTime().equals(yogaClass.getStartTime()))
				&& 
				( currentClass.getEndTime().isBefore(yogaClass.getEndTime()) 
				|| currentClass.getEndTime().equals(yogaClass.getEndTime()));
	}

	private boolean checkOverlappingEndTime(final YogaClass yogaClass, final YogaClass currentClass) {
		return currentClass.getStartTime().isBefore(yogaClass.getEndTime())
				&& currentClass.getEndTime().isAfter(yogaClass.getEndTime());
	}

	private boolean checkOverlappingStartTime(final YogaClass yogaClass, final YogaClass currentClass) {
		return currentClass.getStartTime().isBefore(yogaClass.getStartTime())
				&& currentClass.getEndTime().isAfter(yogaClass.getStartTime());
	}
	
	private void excludeCompleteCanceledAndExpiredClassesFrom(final Query query) {
		final List<String> excludedStatuses = Arrays.asList("Completed", "Cancelled", "Expired");		
		query.setParameter("excludedStatuses", excludedStatuses);
	}
}
