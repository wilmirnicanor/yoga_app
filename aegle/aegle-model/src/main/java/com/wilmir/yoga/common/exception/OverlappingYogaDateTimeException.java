package com.wilmir.yoga.common.exception;

import javax.ejb.ApplicationException;


@ApplicationException
public class OverlappingYogaDateTimeException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3124151943386444559L;
	
	
	public OverlappingYogaDateTimeException(final String message) {
		super(message);
	}
		
}
