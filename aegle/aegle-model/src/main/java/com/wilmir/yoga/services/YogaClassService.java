package com.wilmir.yoga.services;

import java.util.Collection;

import javax.ejb.Local;

import com.wilmir.yoga.common.exception.*;
import com.wilmir.yoga.model.YogaClass;

@Local
public interface YogaClassService {

	YogaClass createClass(final YogaClass yogaClass, final int teacherID) throws FieldNotValidException, OverlappingYogaDateTimeException;
	
	YogaClass findById(final int yogaClassId) throws YogaClassNotFoundException;
	
	Collection<YogaClass> findAllClasses();
	
	Collection<YogaClass> findAllClassesByTeacherID(final int teacherID) throws UserNotFoundException;
		
	YogaClass bookAYogaClass(final int yogaClassId, final int studentID) throws YogaClassNotFoundException, UserNotFoundException, YogaClassFullException, UnauthorizedReservationException;
	
	Collection<YogaClass> searchYogaClasses(final String searchKey);
	
	Collection<YogaClass> getCategory(final String category);
	
	YogaClass cancelYogaClassByTeacher(final int yogaClassId, int teacherId) throws YogaClassNotFoundException, LateCancellationException, UnauthorizedCancellationException, YogaClassOngoingException;

	YogaClass completeYogaClassByTeacher(int yogaClassId, int teacherId) throws YogaClassNotFoundException, UnauthorizedCompletionException;
}
