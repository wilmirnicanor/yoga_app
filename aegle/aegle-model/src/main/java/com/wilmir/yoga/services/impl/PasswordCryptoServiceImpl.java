package com.wilmir.yoga.services.impl;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.ejb.Stateless;

import com.wilmir.yoga.services.PasswordCryptoService;

@Stateless
public class PasswordCryptoServiceImpl implements PasswordCryptoService{

	@Override
	public String encrypt(final String password) {
		MessageDigest messageDigest = null;
		
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
			messageDigest.update(password.getBytes("UTF-8"));
			return  Base64.getEncoder().encodeToString(messageDigest.digest());
		}catch(NoSuchAlgorithmException | UnsupportedEncodingException exception) {
			exception.printStackTrace();
			return null;
		}
		 
	}

}
