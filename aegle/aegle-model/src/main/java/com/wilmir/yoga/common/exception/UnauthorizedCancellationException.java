package com.wilmir.yoga.common.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class UnauthorizedCancellationException extends RuntimeException{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6377645143200651507L;

	public UnauthorizedCancellationException(final String message) {
		super(message);
	}

}
