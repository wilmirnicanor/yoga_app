package com.wilmir.yoga.common.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class UserNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4121218583915982153L;

	public UserNotFoundException(final String message) {
		super(message);
	}
}
