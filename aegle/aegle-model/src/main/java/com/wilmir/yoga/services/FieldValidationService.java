package com.wilmir.yoga.services;

import javax.ejb.Local;

import com.wilmir.yoga.common.exception.FieldNotValidException;

@Local
public interface FieldValidationService {
	<T> void validateFields(final T type) throws FieldNotValidException;
}
