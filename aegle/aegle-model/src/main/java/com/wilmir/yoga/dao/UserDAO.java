package com.wilmir.yoga.dao;

import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.wilmir.yoga.model.Student;
import com.wilmir.yoga.model.Teacher;
import com.wilmir.yoga.model.User;

@Stateless
public class UserDAO {

	@PersistenceContext
	EntityManager entityManager;
	
	
	public User add(final User user) {
		entityManager.persist(user);
		return user;
	}
	
	
	public Optional<User> findById(final int userId) {
		return Optional.ofNullable(entityManager.find(User.class, userId));
	}	
	
	
	public Optional<User> findByEmail(final String email) {
		final Query query = entityManager.createQuery("Select u from User u where LOWER(u.email) = :email", User.class);
		
		query.setParameter("email", email.toLowerCase());
		
		return getUserOptional(query);
	}	
		
	@SuppressWarnings("unchecked")
	public List<Teacher> findAllTeachers(){
		final Query query = entityManager.createQuery("Select DISTINCT u from Teacher u where TYPE(u) = Teacher", Teacher.class);
		return  query.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Student> findAllStudents(){
		final Query query = entityManager.createQuery("Select DISTINCT u from Student u where TYPE(u) = Student", Student.class);
		return  query.getResultList();
	}
	
	
	public boolean existsByEmail(final String email) {
		final Query query = entityManager.createQuery("Select u from User u where u.email = :email", User.class);
		
		return query.setParameter("email", email).getResultList().size() > 0;
	}
	
	
	public Student update(final Student student) {
		return entityManager.merge(student);
	}
	
	
	private Optional<User> getUserOptional(final Query query) {
		User user;
		try {
			user =  (User) query.getSingleResult();
		}catch(NoResultException e) {
			user = null;
		}
		
		return Optional.ofNullable(user);
	}
	
}
