package com.wilmir.yoga.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("teacher")
public class Teacher extends User{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6024670748939069245L;

	public Teacher() {
		
	}
	
	public Teacher(final String name, final String email, final String password, final String image) {
		super(name, email, password, image);
		this.userType = "teacher";
	}
	
}
