package com.wilmir.yoga.services.impl;

import java.util.Iterator;

import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import com.wilmir.yoga.common.exception.FieldNotValidException;
import com.wilmir.yoga.services.FieldValidationService;

@Stateless
public class FieldValidationServiceImpl implements FieldValidationService {

	@Inject
	Validator validator;
	
	@Override
	public <T> void validateFields(final T type) throws FieldNotValidException{
		final Set<ConstraintViolation<T>> errors = validator.validate(type);
		final Iterator<ConstraintViolation<T>> itErrors = errors.iterator();
		if (itErrors.hasNext()) {
			final ConstraintViolation<T> violation = itErrors.next();
			throw new FieldNotValidException(violation.getPropertyPath().toString(), violation.getMessage());
		}	
	}

}
