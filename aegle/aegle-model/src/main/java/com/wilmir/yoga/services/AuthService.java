package com.wilmir.yoga.services;

import java.util.Optional;

import javax.ejb.Local;

import com.wilmir.yoga.model.User;

@Local
public interface AuthService {
	boolean authenticate(final String email, final String password);

	Optional<User> getUser(final String email);
}