package com.wilmir.yoga.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wilmir.yoga.common.serializers.LocalDateTimeDeserializer;
import com.wilmir.yoga.common.serializers.LocalDateTimeSerializer;

@Entity
@Table(name = "yogaClass")
public class YogaClass implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1643873165525569767L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@NotBlank
	private String yogaType;
	
	@NotBlank
	private String description;
	
	@NotNull
	@Min(1)
	private int capacity;
	
	@NotBlank
	private String status;
	
	@NotNull
	@Type(type= "org.hibernate.type.LocalDateTimeType")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)  
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime createdTime;
	
	@NotNull
	@Type(type= "org.hibernate.type.LocalDateTimeType")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)  
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime startTime;
	
	@NotNull
	@Type(type= "org.hibernate.type.LocalDateTimeType")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)  
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime endTime;
	
	@NotBlank
	private String location;
	
	@NotBlank
	@Lob
	private String imageBase64Encoding;
	
	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
	@JoinColumn(name="teacherId", referencedColumnName="id")
	private Teacher teacher;
	
	@ManyToMany(mappedBy = "classes", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch=FetchType.EAGER)
	private final Set<Student> students = new HashSet<>();

	public YogaClass() {
		
	}
	
	public YogaClass(final String yogaType, final String description, final int capacity, final String location, 
			final LocalDateTime startTime, final LocalDateTime endTime, final String imageBase64Encoding) {
		this.yogaType = yogaType;
		this.description = description;
		this.capacity = capacity;
		this.location = location;
		this.createdTime = LocalDateTime.now();
		this.startTime = startTime;
		this.endTime = endTime;
		this.imageBase64Encoding = imageBase64Encoding;
		this.status = "New";
	}
	
	public int getId() {
		return id;
	}

	public void setId(final int yogaId) {
		this.id = yogaId;
	}

	public String getYogaType() {
		return yogaType;
	}

	public void setYogaType(final String yogaType) {
		this.yogaType = yogaType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(final int capacity) {
		this.capacity = capacity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public LocalDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(final LocalDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(final LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(final LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(final String location) {
		this.location = location;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(final Teacher teacher) {
		this.teacher = teacher;
	}

	public Set<Student> getStudents() {
		return students;
	}

	@JsonIgnore
	public boolean isFull() {
		return students.size() >= capacity;
	}
	
	@JsonIgnore
	public boolean isEmpty() {
		return students.isEmpty();
	}
	
	@JsonIgnore
	public boolean isReady() {
		return ChronoUnit.MINUTES.between(LocalDateTime.now(), startTime) <= 60;
	}
	
	@JsonIgnore
	public boolean isOngoing() {
		return LocalDateTime.now().isAfter(startTime) && LocalDateTime.now().isBefore(endTime);
	}
	
	@JsonIgnore
	public boolean isExpired() {
		return endTime.isEqual(LocalDateTime.now()) || endTime.isBefore(LocalDateTime.now());
	}
	
	@JsonIgnore
	public int getTeacherId() {
		return teacher.getId();
	}
	
	public boolean hasStudent(final Student student) {
		return students.contains(student);
	}
	
	
	
	public String getImageBase64Encoding() {
		return imageBase64Encoding;
	}

	public void setImageBase64Encoding(final String imageBase64Encoding) {
		this.imageBase64Encoding = imageBase64Encoding;
	}

	public void add(final Student student) {
		if(students.size() < capacity) {
			this.students.add(student);
			student.add(this);
			
			if(("New").equals(status)) {
				status = "Open";
			}
			
			if(students.size() == capacity) {
				status = "Full";
			}
		}
	}
	

	public void remove(final Student student) {
		if(!students.isEmpty()) {
			students.remove(student);
			student.remove(this);
			
			if(("Full").equals(status)) {
				status = "Open";
			}
		}
	}
	
	@JsonIgnore
	public boolean containsStudent(final Student student) {
		return students.contains(student);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final YogaClass other = (YogaClass) obj;		
		return this.id == other.id;
	}
	

}
	
	
	
