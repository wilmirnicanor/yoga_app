package com.wilmir.yoga.common.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class UnauthorizedCompletionException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1583072064919717154L;

	public UnauthorizedCompletionException(final String message) {
		super(message);
	}

}
