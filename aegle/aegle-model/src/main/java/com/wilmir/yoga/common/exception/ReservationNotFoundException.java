package com.wilmir.yoga.common.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class ReservationNotFoundException extends RuntimeException{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ReservationNotFoundException(final String message) {
		super(message);
	}

}
