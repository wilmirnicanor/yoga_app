package com.wilmir.yoga.common.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class LateCancellationException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3946937993302108622L;
	
	public LateCancellationException(final String message) {
		super(message);
	}

}
