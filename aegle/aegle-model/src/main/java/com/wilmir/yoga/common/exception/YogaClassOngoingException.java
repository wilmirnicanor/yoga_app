package com.wilmir.yoga.common.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class YogaClassOngoingException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5682577276454193572L;

	public YogaClassOngoingException(final String message) {
		super(message);
	}
}
