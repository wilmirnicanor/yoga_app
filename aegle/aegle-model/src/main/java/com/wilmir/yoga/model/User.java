package com.wilmir.yoga.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "users")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="user_type", discriminatorType = DiscriminatorType.STRING)
public class User implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5116410846267251907L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@NotNull
	private String name;
	
	@NotNull
	private String email;
	
	@JsonIgnore
	@NotNull
	private String password;

	@Column(name="user_type", insertable = false, updatable = false)
	protected String userType;	
	
	
	private String image;
	
	public User() {
		
	}
	
	public User(final String name, final String email, final String password, final String image) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.image = image;
	}

	public int getId() {
		return id;
	}

	public void setId(final int userId) {
		this.id = userId;
	}

	public String getName() {
		return name;
	}


	public String getEmail() {
		return email;
	}
	
	public void setEmail(final String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}
	
	public String getUserType() {
	    return userType;
	}
	
	public String getImage() {
		return image;
	}

	public void setImage(final String image) {
		this.image = image;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final User other = (User) obj;
		return compareEmailAndPassword(other);
	}


	private boolean compareEmailAndPassword(final User other) {
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (password == null) {
			if (other.password != null) {
				return false;
			}
		} else if (!password.equals(other.password)) {
			return false;
		}
		return true;
	}

}
