package com.wilmir.yoga.services.impl;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.wilmir.yoga.common.exception.*;
import com.wilmir.yoga.dao.UserDAO;
import com.wilmir.yoga.dao.YogaClassDAO;
import com.wilmir.yoga.model.Student;
import com.wilmir.yoga.model.Teacher;
import com.wilmir.yoga.model.User;
import com.wilmir.yoga.model.YogaClass;
import com.wilmir.yoga.services.FieldValidationService;
import com.wilmir.yoga.services.YogaClassService;

@Stateless
public class YogaClassServiceImpl implements YogaClassService{
	
	
	@Inject
	YogaClassDAO yogaClassDAO;
	
	
	@Inject
	UserDAO userDAO;
	
	
	@Inject
	FieldValidationService validationService;
	
	
	@Override
	public YogaClass createClass(final YogaClass yogaClass, final int teacherID) throws FieldNotValidException, UserNotFoundException, OverlappingYogaDateTimeException{
		final Optional<User> teacherOptional = userDAO.findById(teacherID);

		if(!teacherOptional.isPresent()) {
			throw new UserNotFoundException("The teacher is not found");
		}

		if(yogaClassDAO.findOverlappingClasses(yogaClass, teacherID)) {
			throw new OverlappingYogaDateTimeException("An existing class overlaps with the new class.");
		}
		
		validate(yogaClass);
		
		return yogaClassDAO.add(yogaClass, teacherID); 
	}

	
	@Override
	public YogaClass bookAYogaClass(final int yogaClassId, final int studentID) throws YogaClassNotFoundException, UserNotFoundException, YogaClassFullException, UnauthorizedReservationException{
		final Student student = findStudentById(studentID);
		
		final YogaClass yogaClass = findById(yogaClassId);
		
		if(yogaClass.hasStudent(student)) {
			throw new YogaClassAlreadyBookedException("The student has already booked the class.");
		}
		
		if(yogaClass.isOngoing()) {
			throw new YogaClassOngoingException("The requested yoga class is on-going");
		}
		
		
		if(yogaClass.isFull()) {
			throw new YogaClassFullException("The requested yoga class is full");
		}
				
		yogaClass.add(student);
		
		userDAO.update(student);
		
		return yogaClass;
	}
	
	
	public YogaClass cancelYogaClassByTeacher(final int yogaClassId, final int teacherId) throws YogaClassNotFoundException, LateCancellationException, UnauthorizedCancellationException{
		final YogaClass yogaClass = findById(yogaClassId);
		
		if(yogaClass.isReady() && !yogaClass.isEmpty()) {
			throw new LateCancellationException("Cancellation cannot be made an hour before a class with students");
		}
		
		if(yogaClass.getTeacherId() != teacherId) {
			throw new UnauthorizedCancellationException("The cancellation of this class is forbidden.");
		}
		
		yogaClass.setStatus("Cancelled");
		
		return yogaClassDAO.updateYogaClass(yogaClass);
	}
	
	@Override
	public YogaClass completeYogaClassByTeacher(final int yogaClassId, final int teacherId)
			throws YogaClassNotFoundException, UnauthorizedCompletionException {
		final YogaClass yogaClass = findById(yogaClassId);
		
		if(yogaClass.getTeacherId() != teacherId) {
			throw new UnauthorizedCompletionException("The completion of this class is forbidden.");
		}
		
		yogaClass.setStatus("Completed");
		
		
		return yogaClassDAO.updateYogaClass(yogaClass);	
	}
	
	
	@Override
	public YogaClass findById(final int yogaClassId) throws YogaClassNotFoundException{
		final Optional<YogaClass> yogaClassOptional =  yogaClassDAO.findById(yogaClassId);

		if(yogaClassOptional.isPresent()){
			return yogaClassOptional.get();
		}else {
			throw new YogaClassNotFoundException("The requested yoga class is not found");
		}
	}
	

	@Override
	public Collection<YogaClass> findAllClasses() {		
		final Collection<YogaClass> yogaClasses = sortedByDate(yogaClassDAO.findAllClasses());
		updateClassStatus(yogaClasses);
		return yogaClasses;				
	}
	
	
	@Override
	public Collection<YogaClass> findAllClassesByTeacherID(final int teacherID) throws UserNotFoundException {
		final Optional<User> teacherOptional = userDAO.findById(teacherID);

		if(!teacherOptional.isPresent()) {
			throw new UserNotFoundException("The teacher is not found");
		}
		
		final Collection<YogaClass> yogaClasses = sortedByDate(yogaClassDAO.findAllClassesByTeacherID(teacherID));
		
		updateClassStatus(yogaClasses);

		return yogaClasses;		
	}
	

	@Override
	public Collection<YogaClass> searchYogaClasses(final String searchKey) {
		final Collection<YogaClass> yogaClasses = sortedByDate(yogaClassDAO.searchClasses(searchKey));
		
		updateClassStatus(yogaClasses);
		
		return yogaClasses;
	}
	
	@Override
	public Collection<YogaClass> getCategory(final String category) {
		final Collection<YogaClass> yogaClasses = sortedByDate(yogaClassDAO.getCategory(category));
		
		updateClassStatus(yogaClasses);

		return yogaClasses;
	}
	
	
	private Collection<YogaClass> sortedByDate(final Collection<YogaClass> yogaClasses) {
		return yogaClasses.stream()
				.sorted((currentClass, nextClass)->{
					if(currentClass.getStartTime().isAfter(nextClass.getStartTime())) {
						return 1;
					}else{
						return -1;
					}
				})
				.collect(Collectors.toList());
	}
	
	private void updateClassStatus(final Collection<YogaClass> yogaClasses) {
		yogaClasses.forEach(yogaClass -> {
			final String status = yogaClass.getStatus();
			if(yogaClass.isExpired() && !("Completed").equals(status)) {
				yogaClass.setStatus("Expired");
				yogaClassDAO.updateYogaClass(yogaClass);
			}else if(yogaClass.isOngoing() && ("New".equals(status)||"Open".equals(status)||"Full".equals(status)||"Ready".equals(status))) {
				yogaClass.setStatus("Ongoing");
				yogaClassDAO.updateYogaClass(yogaClass);
			}else if(yogaClass.isReady() && ("New".equals(status)||"Open".equals(status))) {
				yogaClass.setStatus("Ready");
				yogaClassDAO.updateYogaClass(yogaClass);
			}
		});
	}
	
	
	private void validate(final YogaClass yogaClass) throws FieldNotValidException{	
		final LocalDateTime startTime = yogaClass.getStartTime();
		final LocalDateTime endTime = yogaClass.getEndTime();
		if(startTime.isBefore(LocalDateTime.now()) || startTime.isEqual(endTime) || startTime.isAfter(endTime)) {
			throw new FieldNotValidException("startTime", "The start time should be after the end time of the class.");
		}
		validationService.validateFields(yogaClass);
	}
	

	private Student findStudentById(final int studentID) {
		final Optional<User> studentOptional = userDAO.findById(studentID);
		if(!studentOptional.isPresent()) {
			throw new UserNotFoundException("The student is not found");
		}
		
		final User user = studentOptional.get();
		
		if(user instanceof Teacher) {
			throw new UnauthorizedReservationException("Only students can book a class");
		}
		
		return (Student) user;
	}

}
