package com.wilmir.yoga.services.impl;

import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.wilmir.yoga.dao.UserDAO;
import com.wilmir.yoga.model.User;
import com.wilmir.yoga.services.AuthService;
import com.wilmir.yoga.services.PasswordCryptoService;

@Stateless
public class AuthServiceImpl implements AuthService{

	@Inject
	UserDAO userDAO;
	
	@Inject
	PasswordCryptoService passwordService;
	
	@Override
	public boolean authenticate(final String email, final String password) {
		
		if(!isValid(email) || !isValid(password)) {
			return false;
		};
		
		final Optional<User> userOptional = userDAO.findByEmail(email);
		
		if(!userOptional.isPresent()) {
			return false;
		}
		
		final User user = userOptional.get();
		
		final String encryptedPassword = passwordService.encrypt(password);
		
		
		if(encryptedPassword == null) {
			return false;
		}
		
		return (user.getPassword().equals(encryptedPassword));
	}

	
	@Override
	public Optional<User> getUser(final String email) {
		return userDAO.findByEmail(email);
	}
	
	private boolean isValid(final String credential) {
		return credential != null && !credential.isEmpty();
	}

}
