package com.wilmir.yoga.common.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class YogaClassNotFoundException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2486466303883080380L;

	public YogaClassNotFoundException(final String message) {
		super(message);
	}
}
