package com.wilmir.yoga.common.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class YogaClassAlreadyBookedException extends RuntimeException{


	/**
	 * 
	 */
	private static final long serialVersionUID = -652375182666141334L;

	public YogaClassAlreadyBookedException(final String message) {
		super(message);
	}
}
