package com.wilmir.yoga.common.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class YogaClassFullException extends RuntimeException{


	/**
	 * 
	 */
	private static final long serialVersionUID = -910305631002686330L;

	public YogaClassFullException(final String message) {
		super(message);
	}
}
