package com.wilmir.yoga.services;

import javax.ejb.Local;

@Local
public interface PasswordCryptoService {
	String encrypt(final String password);
}
