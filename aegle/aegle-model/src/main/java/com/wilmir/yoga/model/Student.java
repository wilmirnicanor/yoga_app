package com.wilmir.yoga.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@DiscriminatorValue("student")
public class Student extends User{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5891659465380429206L;
	
	@JsonIgnore
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(
			name = "reservations",
			joinColumns = {@JoinColumn(name = "studentId")},
			inverseJoinColumns = {@JoinColumn(name = "classId")}
			)
	private final Set<YogaClass> classes = new HashSet<>();
	
	public Student() {
		
	}
	
	public Student(final String name, final String email, final String password, final String image) {
		super(name, email, password, image );
		this.userType = "student";
	}	

	public Set<YogaClass> getClasses() {
		return classes;
	}
	
	public void add(final YogaClass yogaClass) {
		classes.add(yogaClass);
	}

	public void remove(final YogaClass yogaClass) {
		classes.remove(yogaClass);
	}


	@Override
	public boolean equals(final Object obj) {
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
}
