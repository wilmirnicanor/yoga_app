package com.wilmir.yoga.common.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class UnauthorizedReservationException extends RuntimeException{

	

	/**
	 * 
	 */
	private static final long serialVersionUID = -4858047918570548628L;

	public UnauthorizedReservationException(final String message) {
		super(message);
	}

}
