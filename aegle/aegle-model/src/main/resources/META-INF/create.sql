CREATE TABLE `User` (
  `user_type` varchar(31) NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `yogaClass` (
  `id` int NOT NULL AUTO_INCREMENT,
  `capacity` int NOT NULL,
  `createdTime` tinyblob NOT NULL,
  `endTime` tinyblob NOT NULL,
  `location` varchar(255) NOT NULL,
  `startTime` tinyblob NOT NULL,
  `status` varchar(255) NOT NULL,
  `yogaType` varchar(255) NOT NULL,
  `teacherId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_dmnalfgukjvbm3lvleqscwjv0` (`teacherId`),
  CONSTRAINT `FK_dmnalfgukjvbm3lvleqscwjv0` FOREIGN KEY (`teacherId`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE DATABASE yoga;
CREATE TABLE `bookings` (
  `studentId` int NOT NULL,
  `classId` int NOT NULL,
  PRIMARY KEY (`studentId`,`classId`),
  KEY `FK_1j59rif86qaodwnksrncbb3k8` (`classId`),
  CONSTRAINT `FK_1j59rif86qaodwnksrncbb3k8` FOREIGN KEY (`classId`) REFERENCES `yogaClass` (`id`),
  CONSTRAINT `FK_at62ekcaeyhgthxgvjwuvvqk0` FOREIGN KEY (`studentId`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
