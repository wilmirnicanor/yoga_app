#Author: Wilmir Nicanor <A00278899@student.ait.ie>
@ViewYogaClasses @LoggedInInstructor
Feature: As an instructor
I need to view the list of all my classes
So I can quickly see my upcoming classes.

  Scenario Outline: View Existing Upcoming Classes
    Given The instructor "<instructor>" is logged in
    When The instructor clicks on Manage Upcoming Classes
    Then The list of the upcoming classes is displayed

 	Examples: 
			| instructor 		|
			| Adrienne Lyon |
			 

  Scenario Outline: View Class History
    Given The instructor "<instructor>" is logged in
    When The instructor clicks on My Class History
    Then The list of class history by the instructor is displayed   

	 Examples: 
			| instructor 		|
			| Adrienne Lyon |