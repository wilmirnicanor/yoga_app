#Author: Wilmir Nicanor <A00278899@student.ait.ie>
@ViewAYogaClassInstructor @LoggedInInstructor
Feature: As an instructor
I need to view a class
So, I can see the list of users who booked it.

  Scenario Outline: View A Yoga Class
    Given The instructor "<instructor>" is logged in
    When The instructor clicks on Manage Upcoming Classes
    And The user clicked on a specific yoga class on the upcoming classses
    Then The yoga class detail view "<title>" for the instructor is shown
    And The duration "<duration>" of the class is displayed
    And The number of participants "<participants>" is displayed
    

 	Examples: 
			| instructor 		| title                                             | duration        | participants   |
			| Adrienne Lyon | Vinyasa Class for 5 students in Galway, Ireland   |    45 mins      | 4 participants |
			
			 
