#Author: Wilmir Nicanor <A00278899@student.ait.ie>
@CancelAYogaClassInstructor @LoggedInInstructor
Feature: As an instructor
I need to view a class
So, I can see the list of users who booked it.

  Scenario Outline: Cancel A Yoga Class
    Given The instructor "<instructor>" is logged in
    When The instructor clicks on Manage Upcoming Classes
    And The instructor clicked on a specific yoga class on the upcoming classses they wish to cancel
		And The instructor clicked on the cancel button		
		And The instructor confirmed the cancellation
		And The cancellation confirmation message "<confirmationMessage>" for the instructor is shown
    

 	Examples: 
			| instructor 		| confirmationMessage          |
			| Adrienne Lyon | This class is now canceled   |    
			
			 
