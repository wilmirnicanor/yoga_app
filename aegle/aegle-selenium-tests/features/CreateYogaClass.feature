#Author: Wilmir Nicanor <A00278899@student.ait.ie>
@CreateYogaClass @LoggedInInstructor
Feature: As an instructor,
	I need to be able to create a class,
	So the users can see the available yoga classes I offer

  Scenario Outline: Create A Valid Class
    Given The instructor "<instructor>" is logged in
    And The instructor opens the new class form
    When The instructor selects from the yoga type "<yogaType>" dropdown
    And Entered the description "<description>"
    And Entered the location "<location>"
    And Entered the class capacity "<capacity>"
    And Entered the start date "<startDate>"
    And Entered the start time "<startTime>"
    And Entered the end date "<endDate>"
    And Entered the end time "<endTime>"
    And Uploaded a photo "<photoLocation>"
    And The instructor clicked on create button
    Then The message "<message>" is displayed

    Examples: 
			| instructor 		 | yogaType | description 				| location | capacity | startDate | startTime | endDate | endTime | photoLocation                    | message         	                |
			| Adrienne Lyon  | Bikram  | set your intentions | Dublin   |   15     |  18032022 |    0900   | 18032022|   0930  | /resources/testdatasets/yoga.jpeg| Your new class has beed created! |
			       
			       
  Scenario Outline: Create A Class With Overlapping Schedule
      Given The instructor "<instructor>" is logged in
    	When The instructor opens the new class form
      And The instructor selects from the yoga type "<yogaType>" dropdown
	    And Entered the description "<description>"
	    And Entered the location "<location>"
	    And Entered the class capacity "<capacity>"
	    And Entered the start date "<newStartDate>"
	    And Entered the start time "<newStartTime>"
	    And Entered the end date "<newEndDate>"
	    And Entered the end time "<newEndTime>"
	    And Uploaded a photo "<photoLocation>"
	    And The instructor clicked on create button
	    Then The error message "<errorMessage>" is displayed
	    
	   Examples: 
			| startDate | startTime | endDate | endTime | instructor 		 | yogaType | description 				| location | capacity | newStartDate | newStartTime | newEndDate | newEndTime | photoLocation                    | errorMessage         	                               |
			|  22032022 |    1000   | 22032022|   1100  | Adrienne Lyon | Vinyasa  | set your intentions | Dublin   |   15     |  22032022    |    1010      | 22032022|   1045     | /resources/testdatasets/yoga.jpeg| You have an overlapping class on the dates given      |
      