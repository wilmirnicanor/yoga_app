#Author: Wilmir Nicanor <A00278899@student.ait.ie>
@Login
Feature: As a user,
	I need to log in
	So I can start to use the application


  Scenario Outline: Successful Login
    Given The user is on the landing page
    When The user entered the username"<username>"
    And The user entered the password "<password>"
    And The user clicked on login
    Then The login message "<message>" is displayed
    And The "<fullName>" of the user is displayed as logged in
  

    Examples: 
			| username 		     | password | message 				      | fullName        |
			| wilmir@aegle.com | 123456   | You are now logged in. | Wilmir Nicanor  |
			       
			       
  Scenario Outline: Unsuccessful Login
    Given The user is on the landing page
    When The user entered the username"<username>"
    And The user entered the password "<password>"
    And The user clicked on login
    Then The login error message "<message>" is displayed
	    
	   Examples: 
			| username 		     | password | message 				           |
			| wilmir@aegle.com | 999999   | The user is not recognized. |