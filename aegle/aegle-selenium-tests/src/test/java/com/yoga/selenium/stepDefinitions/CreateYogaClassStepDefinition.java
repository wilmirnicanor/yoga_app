package com.yoga.selenium.stepDefinitions;

import static org.junit.jupiter.api.Assertions.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateYogaClassStepDefinition {
	@Given("^The instructor \"([^\"]*)\" is logged in$")
	public void the_instructor_is_logged_in(String expectedUsername) throws Throwable {
		Thread.sleep(1800);
		final String actualUsername = Hooks.driver.findElement(By.id("logged-in-username")).getAttribute("innerText");
		assertEquals(expectedUsername, actualUsername);
		Thread.sleep(1800);
	}
		
	@Given("^The instructor opens the new class form$")
	public void the_instructor_opens_the_new_class_form() throws Throwable {
		Hooks.driver.findElement(By.cssSelector("[data-target=\"new-class\"]")).click();
		final WebElement yogaClassForm = Hooks.driver.findElement(By.id("yoga-class-form"));
		Thread.sleep(1800);
		assertNotNull(yogaClassForm);	
	}

	@When("^The instructor selects from the yoga type \"([^\"]*)\" dropdown$")
	public void the_instructor_selects_from_the_yoga_type_dropdown(String selectedYogaType) throws Throwable {
		final Select yogaTypeDropdown = new Select(Hooks.driver.findElement(By.id("selectedYogaType")));
		Thread.sleep(1800);
		yogaTypeDropdown.selectByVisibleText(selectedYogaType);		
	}

	@When("^Entered the description \"([^\"]*)\"$")
	public void entered_the_description(String description) throws Throwable {
		Hooks.driver.findElement(By.id("inputtedClassDescription")).sendKeys(description);		
		Thread.sleep(1800);
	}

	@When("^Entered the location \"([^\"]*)\"$")
	public void entered_the_location(String location) throws Throwable {
		Hooks.driver.findElement(By.id("inputtedLocation")).sendKeys(location);		
		Thread.sleep(1800);
	}

	@When("^Entered the class capacity \"([^\"]*)\"$")
	public void entered_the_class_capacity(String capacity) throws Throwable {
		WebElement inputSpinner = Hooks.driver.findElement(By.xpath("/html/body/div/div[2]/main/div[4]/div/div/div/div[2]/form/div[1]/div[2]/div/div/input"));	
		inputSpinner.sendKeys(capacity);		
	}

	@When("^Entered the start date \"([^\"]*)\"$")
	public void entered_the_start_date(String startDate) throws Throwable {
		WebElement dateTimeInput = Hooks.driver.findElement(By.id("inputtedStartTime"));
		dateTimeInput.sendKeys(startDate);
	}
	
	@When("^Entered the start time \"([^\"]*)\"$")
	public void entered_the_start_time(String startTime) throws Throwable {
		WebElement dateTimeInput = Hooks.driver.findElement(By.id("inputtedStartTime"));
		dateTimeInput.sendKeys(Keys.TAB);
		dateTimeInput.sendKeys(startTime);
	}
	
	@When("^Entered the end date \"([^\"]*)\"$")
	public void entered_the_end_date(String endDate) throws Throwable {
		WebElement dateTimeInput = Hooks.driver.findElement(By.id("inputtedEndTime"));
		dateTimeInput.sendKeys(endDate);
	}

	@When("^Entered the end time \"([^\"]*)\"$")
	public void entered_the_end_time(String endTime) throws Throwable {
		WebElement dateTimeInput = Hooks.driver.findElement(By.id("inputtedEndTime"));
		dateTimeInput.sendKeys(Keys.TAB);
		dateTimeInput.sendKeys(endTime);
	}

	@When("^Uploaded a photo \"([^\"]*)\"$")
	public void uploaded_a_photo(String photoLocation) throws Throwable {
		WebElement photoUpload = Hooks.driver.findElement(By.id("uploadedImage"));
		photoUpload.sendKeys(Hooks.ROOT_DIRECTORY + photoLocation);
		Thread.sleep(1000);
	}

	@When("^The instructor clicked on create button$")
	public void the_instructor_clicked_on_create_button() throws Throwable {
		Hooks.driver.findElement(By.id("create-yoga-class-btn")).click();
		Thread.sleep(2000);
	}
	
	
	@Then("^The message \"([^\"]*)\" is displayed$")
	public void the_message_is_displayed(String expectedMessage) throws Throwable {
		String actualMessage = Hooks.driver.findElement(By.id("successful-yoga-create")).getAttribute("innerText");
		assertEquals(expectedMessage, actualMessage);
	}
	
	
	@Then("^The error message \"([^\"]*)\" is displayed$")
	public void the_error_message_is_displayed(String expectedMessage) throws Throwable {
		String actualMessage = Hooks.driver.findElement(By.id("failed-yoga-create")).getAttribute("innerText");
		assertEquals(expectedMessage, actualMessage);
	}
	
	
	@Given("^The class at \"([^\"]*)\", \"([^\"]*)\" , \"([^\"]*)\", and \"([^\"]*)\" already exists$")
	public void the_class_at_and_already_exists(String startDate, String startTime, String endDate, String endTime) throws Throwable {
		final Select yogaTypeDropdown = new Select(Hooks.driver.findElement(By.id("selectedYogaType")));
		Thread.sleep(3000);
		yogaTypeDropdown.selectByVisibleText("Hatha");	
		Hooks.driver.findElement(By.id("inputtedClassDescription")).sendKeys("Set your intention");	
		Hooks.driver.findElement(By.id("inputtedLocation")).sendKeys("Athlone, Ireland");		
		Hooks.driver.findElement(By.xpath("/html/body/div/div[2]/main/div/div/div[1]/div/div[2]/form/div[2]/div/div/input")).sendKeys("20");
		WebElement startDateInput = Hooks.driver.findElement(By.id("inputtedStartTime"));
		startDateInput.sendKeys(startDate);
		startDateInput.sendKeys(Keys.TAB);
		startDateInput.sendKeys(startTime);
		WebElement endDateInput = Hooks.driver.findElement(By.id("inputtedEndTime"));
		endDateInput.sendKeys(endDate);
		endDateInput.sendKeys(Keys.TAB);
		endDateInput.sendKeys(endTime);
		Hooks.driver.findElement(By.id("uploadedImage")).sendKeys(Hooks.ROOT_DIRECTORY + "/resources/testdatasets/yoga.jpeg");
		Thread.sleep(1000);
		Hooks.driver.findElement(By.id("create-yoga-class-btn")).click();
		Thread.sleep(2000);
	}
}
