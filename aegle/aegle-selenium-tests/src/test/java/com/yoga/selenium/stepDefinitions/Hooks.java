package com.yoga.selenium.stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	public static WebDriver driver;
	public static final String ROOT_DIRECTORY = System.getProperty("user.dir");
	public static final String	AEGLE_PAGE = "http://ec2-54-76-1-191.eu-west-1.compute.amazonaws.com:8080/yoga/";
	
	
	@Before
	public void setup() {
		System.setProperty("webdriver.chrome.driver", ROOT_DIRECTORY + "/resources/chromedriver/chromedriver");
		
		ChromeOptions options = new ChromeOptions();
		
		options.addArguments("--headless", "--window-size=1920,1200");

		driver = new ChromeDriver(options);
		
		Hooks.driver.get(AEGLE_PAGE);
	}
	
	
	@Before("@LoggedInInstructor")
	public void loginSetupForInstructor() throws InterruptedException {
	
		Hooks.driver.findElement(By.id("headerLoginEmail")).sendKeys("adrienne@aegle.com");
		Hooks.driver.findElement(By.id("headerLoginPassword")).sendKeys("123456");
		Hooks.driver.findElement(By.id("headerLogin")).click();
	}
	
	
	@After
	public void tearDown() {
		driver.quit();
	}
}
