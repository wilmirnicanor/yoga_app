package com.yoga.selenium.stepDefinitions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.openqa.selenium.By;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CancelAClassForInstructorStepDefinition {

	@When("^The instructor clicked on a specific yoga class on the upcoming classses they wish to cancel$")
	public void the_instructor_clicked_on_a_specific_yoga_class_on_the_upcoming_classses_they_wish_to_cancel() throws Throwable {
		Thread.sleep(2000);
		Hooks.driver.findElement(By.cssSelector("#teacherUpcomingClassList [data-id=\"6\"]")).click();
	}

	@When("^The instructor clicked on the cancel button$")
	public void the_instructor_clicked_on_the_cancel_button() throws Throwable {
		Thread.sleep(3000);
		Hooks.driver.findElement(By.id("teacherCancelYogaClassBtn")).click();
	}

	@When("^The instructor confirmed the cancellation$")
	public void the_instructor_confirmed_the_cancellation() throws Throwable {
		Thread.sleep(1000);
		Hooks.driver.findElement(By.id("teacherCancelConfirmBtn")).click();
	}

	@Then("^The cancellation confirmation message \"([^\"]*)\" for the instructor is shown$")
	public void the_cancellation_confirmation_message_for_the_instructor_is_shown(String message) throws Throwable {
		Thread.sleep(1000);
		final String actualMessage = Hooks.driver.findElement(By.cssSelector(".toast-message")).getAttribute("innerText");
		assertEquals(message, actualMessage);
	}

}
