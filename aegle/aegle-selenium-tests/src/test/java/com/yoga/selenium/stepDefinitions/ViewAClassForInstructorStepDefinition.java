package com.yoga.selenium.stepDefinitions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.openqa.selenium.By;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ViewAClassForInstructorStepDefinition {

	@When("^The user clicked on a specific yoga class on the upcoming classses$")
	public void the_user_clicked_on_a_specific_yoga_class_on_the_upcoming_classses() throws Throwable {
		Thread.sleep(2000);
		Hooks.driver.findElement(By.cssSelector("#teacherUpcomingClassList [data-id=\"4\"]")).click();
	}
	

	@Then("^The yoga class detail view \"([^\"]*)\" for the instructor is shown$")
	public void the_yoga_class_detail_view_for_the_instructor_is_shown(String title) throws Throwable {
		Thread.sleep(2000);
	   final String actualTitle = Hooks.driver.findElement(By.id("teacherYogaClassDetailTitle")).getAttribute("innerText");
	   assertEquals(title, actualTitle);
	}

	
	@Then("^The duration \"([^\"]*)\" of the class is displayed$")
	public void the_duration_of_the_class_is_displayed(String duration) throws Throwable {
		Thread.sleep(2000);
		final String actualDuration = Hooks.driver.findElement(By.id("teacherYogaClassDetailDuration")).getAttribute("innerText");
		assertEquals(duration, actualDuration);
	}

	@Then("^The number of participants \"([^\"]*)\" is displayed$")
	public void the_number_of_participants_is_displayed(String participants) throws Throwable {
		final String actualParticipants = Hooks.driver.findElement(By.id("teacherClassCapacityStatus")).getAttribute("innerText");
		assertEquals(participants, actualParticipants);
	}

}
