package com.yoga.selenium.stepDefinitions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.openqa.selenium.By;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginStepDefinitions {
	@Given("^The user is on the landing page$")
	public void the_user_is_on_the_landing_page() throws Throwable {
		Thread.sleep(2000);
		final String actualLogo = Hooks.driver.findElement(By.cssSelector(".navbar-brand")).getAttribute("innerText");
		assertEquals("Aegle Yoga", actualLogo);
	}

	
	@When("^The user entered the username\"([^\"]*)\"$")
	public void the_user_entered_the_username(String username) throws Throwable {
		Hooks.driver.findElement(By.id("headerLoginEmail")).sendKeys(username);
	}

	@When("^The user entered the password \"([^\"]*)\"$")
	public void the_user_entered_the_password(String password) throws Throwable {
		Hooks.driver.findElement(By.id("headerLoginPassword")).sendKeys(password);
	}

	@When("^The user clicked on login$")
	public void the_user_clicked_on_login() throws Throwable {
		Hooks.driver.findElement(By.id("headerLogin")).click();
	}


	@Then("^The login message \"([^\"]*)\" is displayed$")
	public void the_login_message_is_displayed(String message) throws Throwable {
		Thread.sleep(1500);
		final String actualMessage = Hooks.driver.findElement(By.className("toast-message")).getAttribute("innerText");
		assertEquals(message, actualMessage);
	}

	@Then("^The \"([^\"]*)\" of the user is displayed as logged in$")
	public void the_of_the_user_is_displayed_as_logged_in(String username) throws Throwable {
		String actualUsername = Hooks.driver.findElement(By.id("logged-in-username")).getAttribute("innerText");
		assertEquals(username, actualUsername);	
	}

	@Then("^The login error message \"([^\"]*)\" is displayed$")
	public void the_login_error_message_is_displayed(String message) throws Throwable {
		Thread.sleep(1500);
		final String actualMessage = Hooks.driver.findElement(By.cssSelector(".toast-message")).getAttribute("innerText");
		assertEquals(message, actualMessage);
	}
}
