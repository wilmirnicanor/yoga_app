package com.yoga.selenium.stepDefinitions;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ViewClassesForInstructorStepDefinition {
	@When("^The instructor clicks on Manage Upcoming Classes$")
	public void the_instructor_clicks_on_Manage_Upcoming_Classes() throws Throwable {
		Hooks.driver.findElement(By.cssSelector("[data-target=\"manage-classes\"]")).click();
	}

	@Then("^The list of the upcoming classes is displayed$")
	public void the_list_of_the_upcoming_classes_is_displayed() throws Throwable {
		final WebElement manageClassesContainer = Hooks.driver.findElement(By.id("manage-classes"));
		assertNotNull(manageClassesContainer);		
		Thread.sleep(1800);
	}

	@When("^The instructor clicks on My Class History$")
	public void the_instructor_clicks_on_My_Class_History() throws Throwable {
		Hooks.driver.findElement(By.cssSelector("[data-target=\"my-class-history\"]")).click();
	}

	@Then("^The list of class history by the instructor is displayed$")
	public void the_list_of_class_history_by_the_instructor_is_displayed() throws Throwable {
		final WebElement classHistoryContainer = Hooks.driver.findElement(By.id("my-class-history"));
		assertNotNull(classHistoryContainer);
	}
	
}
